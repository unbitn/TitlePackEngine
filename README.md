# Unbitn TitlePack Engine

The Unbitn TitlePack Engine allows you to quickly start working on your own TitlePack without the need to create your own boilerplate. Everthing is prepared for you.

## How does it work?

The Unbitn TitlePack Engine works in a similar way as a MVC framework. The Titlepack is divided in several controllers, themselves divided in one or several actions which hold the main logic. Computing things, fetching data, ... These controller actions can return a response to tell the Engine to render a view or to redirect to a new route.

## Included

- **App Base** : extend this code base with your own main script
- **Settings** : configure your TitlePack with JSON settings
- **Event Manager** : dispatch and catch custom events through your modules
- **File** : Send asynchronous file fetching requests and get the result later with an event dispatched with the **Event Manager**
- **Access Control** : Check that your user has the correct privileges to access a controller's action
- **Router** : handles routing through your controllers
- **View Converter** : converts the powerful views to vanilla manialink pages
- **Response Handler** : handles the responses from your controller actions
- **Layers Manager** : Manages the stack of displayed layers

## Coding conventions

This project uses a really precise coding style, make sure to understand and follow the rules below.

### Style
- Curly braces on the same line
- Use PascalCase for nearly everything : variables, functions, files
- Functions not meant to be called from outside the script file (private functions) have to be prefixed with `Private_`
- Constants are prefixed with `C_`
- Global variables are prefixed with `G_`
- Structures are prefixed with `M_` (for "model")

### Structure
- The main script of your TitlePack has to be placed at the root of your namespace.
	- Example: If the namespace is `Nerpson/Pizzeria`, the main script will have the path `Scripts/Libs/Nerpson/Pizzeria/Main.Script.txt`.
- Library scripts can't just be placed in the root of your namespace. They have to be placed in a subfolder with the same name. While it may be strange at first, it will make sense later.
	- Example: a library `Pizza.Script.txt` has to be placed in a `Pizza` folder, making the path `Pizza/Pizza.Script.txt`.
- Constants which are meant to be public have to be separated into another file `Consts.Script.txt` in the library folder. If the constants are not related to any library, put the file directly at the root of your namespace.
	- Example: The public constants of a script `Pizza.Script.txt` have to be declared into the file `Pizza/Consts.Script.txt`
- Constants describing event types for the Event Manager have to be declared in a separate file `Enums/EventType.Script.txt`.
	- Example : If you want to create an event of the type described in the constant : `#Const C_PizzaCooked "Pizza_PizzaCooked"` you have to place this line in the file `Pizza/Enums/EventType.Script.txt` if the event is triggered in the Pizza library.
- If you have multiple constants describing all the possible values for something, separate them into another file in an `Enums` subfolder, in the library folder. If the constants are not related to any library, put the file directly in a `Enums` folder at the root of your namespace.
	- Example: The constants describing all the pizza types (*e.g.* `#Const C_Margherita 0`, `#Const C_Calzone 1`, *etc*) have to be declared into the file `Pizza/Enums/PizzaType.Script.txt` if they depend directly on a Pizza library.
- Structures which are meant to be public have to be separated into their own file
	- Example : A struct `M_Topping` depending on a script `Pizza.Script.txt` has to be declared into the file `Pizza/Models/Topping.Script.txt`

#### Recap
Let's take back the Pizzeria example to make sure everything is clear.
```
📁 Scripts
	📁 Libs
		📁 Nerpson
			📁 Pizzeria
				📁 Pizza
					📁 Enums
						📄 PizzaType.Script.txt
					📁 Models
						📄 Topping.Script.txt
					📄 Consts.Script.txt
					📄 Pizza.Script.txt
				📄 Main.Script.txt
```

### Documentation
- Name everything as precisely as possible (variables, functions, libraries, *etc*)
- Every code you feel a bit tricky to understand has to be accompanied with comments
- Functions have to be documented with JavaDoc using the appropriate tags (*i.e.* `@param`, `@return`, `@code`, *etc*)