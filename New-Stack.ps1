Param (
	[string] $Namespace,
	[string] $Name
)

Import-Module ./Utils.psm1

if ($Namespace -eq "") {
	$Namespace = Get-Namespace
}

if (-Not (Test-Namespace -Namespace $Namespace)) {
	Write-Error -Message "'$Namespace' is not a valid namespace." -Category SyntaxError
	exit
}


if ($Name -eq "") {
	$Name = Read-Host "Enter the name of the stack"
}

if (-Not ($Name -cmatch "^[A-Z]\w*$")) {
	Write-Error -Message "'$Name' is not a valid stack name." -Category SyntaxError
	exit
}

$stacks_file_path = "Scripts/Libs/$Namespace/Enums/Stack.Script.txt"

if (Test-Path $stacks_file_path) {
	# Stacks file exists.
	
	# Loads the file content in a string.
	$stacks_file_content = Get-Content -Path $stacks_file_path -Raw

	# $stacks_file_content = $stacks_file_content.Substring(0, $match.Index - 1) + $include_line + $stacks_file_content.Substring($match.Index)

	# Looks for the first line which doesn't start with a '#'.
	$constant_matches = [regex]::Matches($stacks_file_content, '^#Const(\s+)C_[A-Z]\w*(\s+)"([A-Z]\w*)"\r?$', [System.Text.RegularExpressions.RegexOptions]::Multiline)
	if ($constant_matches.Count -eq 0) {
		Write-Error -Message "No valid stack constant found." -Category SyntaxError
		exit
	}

	$last_match = $constant_matches[$constant_matches.Count - 1]
	$spaces_before = ($last_match.Groups[1].Value[0] -as [string]) * $last_match.Groups[1].Length
	$spaces_after = ($last_match.Groups[2].Value[0] -as [string]) * $last_match.Groups[2].Length
	$new_line = "`n#Const${spaces_before}C_${Name}${spaces_after}`"${Name}`""
	$insert_pos = $last_match.Index + $last_match.Length

	# Merges the file with the new line.
	$stacks_file_content = $stacks_file_content.Substring(0, $insert_pos) + $new_line + $stacks_file_content.Substring($insert_pos)

	# Overwrites the file's content.
	Set-Content -Path $stacks_file_path -Value $stacks_file_content -NoNewline

} else {
	# Stacks file does not exist.
	
	$stacks_file_content = '/**
 * Generated stacks file #NAMESPACE#/Enums/Stack.Script.txt
 * Avoid editing it if you want to use the CLI tools.
 */
#Const C_#NAME# "#NAME#"
'
	$stacks_file_content = $stacks_file_content -creplace '#NAMESPACE#', $Namespace
	$stacks_file_content = $stacks_file_content -creplace '#NAME#', $Name
		
	# Creates the file.
	New-Item -Force -Path $stacks_file_path -ItemType File -Value $stacks_file_content | Out-Null
}
