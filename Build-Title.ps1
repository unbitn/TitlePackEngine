Param(
	[string]$ManiaPlanetPath
)
Import-Module ./Utils.psm1

$title_id = Get-TPIDFromDir -Path $PSScriptRoot
$author = Get-Author -TitlePackId $title_id

# $reg_key = "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Classes\maniaplanet\Shell\Open\Command"
# $data_key = "(default)"

if ($ManiaPlanetPath -eq "") {
	$reg_key = "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\ManiaPlanet_is1"
	$data_key = "InstallLocation"

	$ManiaPlanetPath = (Get-ItemProperty -Path $reg_key | Select-Object $data_key).$data_key
	$ManiaPlanetPath += "ManiaPlanet.exe"
}
$params = "/WindowLess /CreatePack=`"$title_id`" /Login=`"$author`""

Start-Process -NoNewWindow -FilePath $ManiaPlanetPath -ArgumentList $params -Wait
