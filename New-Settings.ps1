Param (
	[string] $Namespace
)

Import-Module ./Utils.psm1

# Namespace name.
if ($Namespace -eq "") {
	$Namespace = Get-Namespace
}

if (-Not (Test-Namespace -Namespace $Namespace)) {
	Write-Error -Message "'$Namespace' is not a valid namespace." -Category SyntaxError
	exit
}

$title_id = Get-TPIDFromDir -Path $PSScriptRoot

$full_path = "Media/Json/$title_id.Settings.json"

if ([System.IO.File]::Exists($full_path)) {
	Write-Error -Message "The file '$full_path' already exists." -Category ResourceExists
	exit
}

$file_content = '{
	"$schema": "./Settings.schema.json",
	"Namespace": "#NAMESPACE#",
	"Env": "development",
	"Version": {
		"Year": #YEAR#,
		"Month": #MONTH#,
		"Day": #DAY#,
		"Build": 1
	},
	"FallbackLocale": "en",
	"Colors": {}
}
'
$file_content = $file_content -replace '#NAMESPACE#', $Namespace
$file_content = $file_content -replace '#YEAR#', [int](Get-Date -Format "yyyy")
$file_content = $file_content -replace '#MONTH#', [int](Get-Date -Format "MM")
$file_content = $file_content -replace '#DAY#', [int](Get-Date -Format "dd")

Write-Host "Creating JSON settings file '$full_path'."

# Creates the folders.
$folder = $full_path | Split-Path
if (-not (Test-Path $folder)) {
	New-Item -Path $folder -ItemType Directory | Out-Null
}

# Creates the file.
New-Item -Path $full_path -ItemType File -Value $file_content | Out-Null

Write-Host "JSON setings file '$full_path' successfully created."
