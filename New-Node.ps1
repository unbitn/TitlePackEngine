Param (
	[string] $Namespace,
	[string] $NodeName
)

Import-Module ./Utils.psm1

# Namespace name.
if ($Namespace -eq "") {
	$Namespace = Get-Namespace
}

if (-Not (Test-Namespace -Namespace $Namespace)) {
	Write-Error -Message "'$Namespace' is not a valid namespace." -Category SyntaxError
	exit
}

# Main script name.
if ($NodeName -eq "") {
	$NodeName = Read-Host "Enter the name of the node"
}

if ($NodeName -cnotmatch "^[A-Z]\w*$") {
	Write-Error -Message "'$NodeName' is not a valid script name." -Category SyntaxError
	exit
}

$full_path = "Scripts/Libs/$Namespace/Nodes/$NodeName.Script.txt"
$file_content = '/**
 * Generated main script #Namespace#/Nodes/#NodeName#.Script.txt
 */
#Include "Libs/Unbitn/TextLib.Script.txt" as TL
#Include "Libs/Unbitn/Engine/Log/Log.Script.txt" as Log
#Include "Libs/Unbitn/Engine/EventManager/EventManager.Script.txt" as EventMgr
#Include "Libs/Unbitn/Engine/Renderer/Enums/EventType.Script.txt" as ERendererEventType
#Include "Libs/Unbitn/Engine/Renderer/Renderer.Script.txt" as Renderer

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeProperties.Script.txt" as NodeProperties
#Struct NodeProperties::M_NodeProperties as M_NodeProperties
#Struct NodeProperties::M_AttributeProperties as M_AttributeProperties

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeToProcessEventData.Script.txt" as NodeToProcessEventData
#Struct NodeToProcessEventData::M_NodeToProcessEventData as M_NodeToProcessEventData

#Const C_ScriptName "Node%NodeName%"

#Const C_NodeProperties M_NodeProperties {
	Name = "%NodeName%",
	CanHaveChildren = False,
	AttributeGroups = [],
	Attributes = []
}

declare Text G_Subscription;

/**
 * Contains the node''s core computing code
 * @param _EventData The caught event.
 */
Void Private_Process(M_NodeToProcessEventData _EventData) {
	Log::Verbose(C_ScriptName, "Processing node.");
	Renderer::Combine(_EventData, """<label text="Hello from %NodeName%!"/>""");
}

/**
 * Checks for NodeToProcess events.
 * Function to call in the main script''s Loop label.
 */
Void Loop() {
	foreach (Event in EventMgr::GetSubscribedEvents(G_Subscription)) {
		declare M_NodeToProcessEventData EventData = NodeToProcessEventData::Deserialize(Event.Data);
		if (TL::CaselessEquals(EventData.NodeName, C_NodeProperties.Name)) {
			Private_Process(EventData);
			EventMgr::Consume(Event);
		}
	}
}

/**
 * Initializes the node and subscribes to the NodeToProcess event.
 * Send this function''s input as argument of Renderer::RegisterNode().
 * @return An M_NodeProperties structure containing information about the node.
 */
M_NodeProperties Init() {
	G_Subscription = EventMgr::Subscribe(ERendererEventType::C_NodeToProcess);
	return C_NodeProperties;
}
'
$file_content = $file_content -replace '%NodeName%', $NodeName

if ([System.IO.File]::Exists($full_path)) {
	Write-Error -Message "The file '$full_path' already exists." -Category ResourceExists
	exit
}

# Editing the main file to add the include line, the registering, and the Loop() call.
$edit_main = Read-Host "Automatically add the lines in your main script? (y/n)"
if ($edit_main -match '^y$') {
	$main_file_name = Read-Host "Enter the main file name (default: Main)"

	if ($main_file_name -eq "") {
		$main_file_name = "Main"
	}
	$main_file_full_path = "Scripts/Libs/$Namespace/$main_file_name.Script.txt"
	if (-not (Test-Path $main_file_full_path)) {
		Write-Error -Message "The file '$main_file_full_path' does not exist." -Category ObjectNotFound
		exit
	}

	# Loads the file content in a string.
	$main_file_content = Get-Content -Path $main_file_full_path -Raw


	# -------- ADD THE INCLUDE LINE -------- #
	
	# Looks for the first line wich doesn't start with a '#'.
	$match = [regex]::Match($main_file_content, '^[^#\s\/]', [System.Text.RegularExpressions.RegexOptions]::Multiline)

	if (-Not ($match.Success)) {
		Write-Error -Message "Couldn't add the include line." -Category ObjectNotFound
		exit
	}

	# Composes the line.
	$include_line = "#Include `"Libs/$Namespace/Nodes/$NodeName.Script.txt`" as Node$NodeName`n`n"

	# Merges the file with the new line.
	$main_file_content = $main_file_content.Substring(0, $match.Index - 1) + $include_line + $main_file_content.Substring($match.Index - 1)


	# -------- ADD THE INIT LINE -------- #
	
	# Looks for the InitNode label line with a RegEx.
	$match = [regex]::Match($main_file_content, '^\s*\*\*\*InitNodes\*\*\*\s*\n(\s*)\*\*\*\s*\r?$', [System.Text.RegularExpressions.RegexOptions]::Multiline)

	if (-Not ($match.Success)) {
		# Looks for the first line wich doesn't start with a '#'.
		$match = [regex]::Match($main_file_content, '^[^#\s\/]', [System.Text.RegularExpressions.RegexOptions]::Multiline)

		# Composes the line.
		$label_line = "***InitNodes***`n***`nRenderer::RegisterNode(Node${NodeName}::Init());`n***`n"
	
		# Merges the file with the new line.
		$main_file_content = $main_file_content.Substring(0, $match.Index - 1) + $label_line + $main_file_content.Substring($match.Index - 1)
	} else {
		# Prepares the indentation.
		$tabs = $match.Groups[1].Value

		# Composes the line.
		$node_init_line = "${tabs}Renderer::RegisterNode(Node${NodeName}::Init());`n"
		
		$match_after = $match.Index + $match.Length

		# Merges the file with the new line.
		$main_file_content = $main_file_content.Substring(0, $match_after) + $node_init_line + $main_file_content.Substring($match_after + 1)
	}


	# -------- ADD THE LOOP LINE -------- #
	
	# Looks for the switch line with a RegEx.
	$match = [regex]::Match($main_file_content, '^\s*\*\*\*Loop\*\*\*\s*\n(\s*)\*\*\*\s*\r?$', [System.Text.RegularExpressions.RegexOptions]::Multiline)

	if (-Not ($match.Success)) {
		Write-Error -Message "Couldn't find the Loop label to add the Node$NodeName::Loop(); line." -Category ObjectNotFound
		exit
	}

	# Prepares the indentation.
	$tabs = $match.Groups[1].Value

	# Composes the line.
	$node_loop_line = "${tabs}Node${NodeName}::Loop();`n"
	
	$match_after = $match.Index + $match.Length

	# Merges the file with the new line.
	$main_file_content = $main_file_content.Substring(0, $match_after) + $node_loop_line + $main_file_content.Substring($match_after + 1)


	# Overwrites the file's content.
	Set-Content -Path $main_file_full_path -Value $main_file_content

	Write-Host "Node lines added to $main_file_full_path"
}

Write-Host "Creating main script file '$full_path'."

# Creates the folders.
$folder = $full_path | Split-Path
if (-not (Test-Path $folder)) {
	New-Item -Path $folder -ItemType Directory | Out-Null
}

# Creates the file.
New-Item -Path $full_path -ItemType File -Value $file_content | Out-Null

Write-Host "Node script '$NodeName' successfully created."
