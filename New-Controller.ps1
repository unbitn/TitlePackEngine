Param (
	[string] $Namespace,
	[string] $ControllerName
)

Import-Module ./Utils.psm1

# Namespace name.
if ($Namespace -eq "") {
	$Namespace = Get-Namespace
}

if (-Not (Test-Namespace -Namespace $Namespace)) {
	Write-Error -Message "'$Namespace' is not a valid namespace." -Category SyntaxError
	exit
}

# Controller name.
if ($ControllerName -eq "") {
	$ControllerName = Read-Host "Enter the controller name (e.g. Home)"
}
if (-Not ($ControllerName -cmatch "^[A-Z]\w*$")) {
	Write-Error -Message "'$ControllerName' is not a valid controller name." -Category SyntaxError
	exit
}

# Editing the main file to add the include line and the switch case.
$edit_main = Read-Host "Automatically add the lines in your main script? (y/n)"
if ($edit_main -match '^y$') {
	$main_file_name = Read-Host "Enter the main file name (default: Main)"

	if ($main_file_name -eq "") {
		$main_file_name = "Main"
	}
	$main_file_full_path = "Scripts/Libs/$Namespace/$main_file_name.Script.txt"
	if (-not (Test-Path $main_file_full_path)) {
		Write-Error -Message "The file '$main_file_full_path' does not exist." -Category ObjectNotFound
		exit
	}

	# Loads the file content in a string.
	$main_file_content = Get-Content -Path $main_file_full_path -Raw

	# Looks for the first line wich doesn't start with a '#'.
	$match = [regex]::Match($main_file_content, '^[^#\s\/]', [System.Text.RegularExpressions.RegexOptions]::Multiline)

	if (-Not ($match.Success)) {
		Write-Error -Message "Couldn't add the include line." -Category ObjectNotFound
		exit
	}

	# Composes the line.
	$include_line = "#Include `"Libs/$Namespace/Controllers/$ControllerName.Script.txt`" as Controller$ControllerName`n`n"

	# Merges the file with the new line.
	$main_file_content = $main_file_content.Substring(0, $match.Index - 1) + $include_line + $main_file_content.Substring($match.Index)
	
	# Looks for the switch line with a RegEx.
	$match = [regex]::Match($main_file_content, '^(\s*)switch\s*\(\s*EventData\.Route\.Controller\s*\)\s*\{\s*\r?$', [System.Text.RegularExpressions.RegexOptions]::Multiline)

	if (-Not ($match.Success)) {
		Write-Error -Message "Couldn't find the event data switch to add the case line." -Category ObjectNotFound
		exit
	}

	# Prepares the indentation.
	$tabs = $match.Groups[1].Value + $match.Groups[1].Value[0]

	# Composes the line.
	$case_line = "${tabs}case `"$ControllerName`": Response = Controller$ControllerName::Action(EventData.Route.Action, EventData.Route.Parameters);`n"
	
	$match_after = $match.Index + $match.Length

	# Merges the file with the new line.
	$main_file_content = $main_file_content.Substring(0, $match_after) + $case_line + $main_file_content.Substring($match_after + 1)

	# Overwrites the file's content.
	Set-Content -Path $main_file_full_path -Value $main_file_content

	Write-Host "Controller lines added to $main_file_full_path"
}

$full_path = "Scripts/Libs/$Namespace/Controllers/$ControllerName.Script.txt"
$file_content = '/**
 * Generated controller %Namespace%/Controllers/%ControllerName%.Script.txt
 */
#Include "Libs/Unbitn/Engine/LayersManager/Enums/DisplayMethod.Script.txt" as EDisplayMethod
#Include "Libs/%Namespace%/Enums/Stack.Script.txt" as EStack

#Include "Libs/Unbitn/Engine/ResponseHandler/Models/View.Script.txt" as View
#Struct View::M_View as M_View

#Include "Libs/Unbitn/Engine/ResponseHandler/Models/Response.Script.txt" as Response
#Struct Response::M_Response as M_Response

#Const C_Id				"%ControllerName%"

M_Response ActionIndex(Text[Text] _Parameters) {
	declare Text View = """
	<view>
		<label text="Hello Controller %ControllerName%!"/>
	</view>""";

	return Response::New(
		View::New(
			C_Id,
			View
		),
		EStack::C_Main,
		EDisplayMethod::C_Set
	);
}

M_Response Action(Text _Action, Text[Text] _Parameters) {
	switch (_Action) {
		case "Index", "": return ActionIndex(_Parameters);
	}

	assert(False, """Unhandled action {{{ dump(_Action) }}}.""");
	return M_Response {};
}
'
$file_content = $file_content -replace '%Namespace%', $Namespace
$file_content = $file_content -replace '%ControllerName%', $ControllerName

if ([System.IO.File]::Exists($full_path)) {
	Write-Error -Message "The file '$full_path' already exists." -Category ResourceExists
	exit
}

Write-Host "Creating controller file '$full_path'."

# Creates the folders.
$folder = $full_path | Split-Path
if (-not (Test-Path $folder)) {
	New-Item -Path $folder -ItemType Directory | Out-Null
}

# Creates the file.
New-Item -Path $full_path -ItemType File -Value $file_content | Out-Null

Write-Host "Controller '$ControllerName' successfully created."
