function Get-Namespace {
	$Namespace = Read-Host "Enter the namespace (e.g. Hylis/MyFirstGame)"
	return $Namespace
}

function Test-Namespace {
	Param (
		[string] $Namespace
	)
	
	return ($Namespace -cmatch '^[A-Z]\w*(?:\/[A-Z]\w*)*$')
}

function Test-TranslationId {
	Param (
		[string] $Id
	)

	return ($Id -cmatch '^[A-Z]\w*(?:\.[A-Z]\w*)*$')
}

function Get-TPIDFromDir {
	Param(
		[string] $Path
	)
	
	return (Split-Path $Path -Leaf)
}

function Get-Author {
	Param(
		[string] $TitlePackId
	)

	return $TitlePackId.Split("@")[1]
}
