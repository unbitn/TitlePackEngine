#Struct M_Date {
	Integer Year;
	Integer Month;
	Integer Day;
}

#Const C_IsValidCode_Success			0
#Const C_IsValidCode_InvalidMonth	1
#Const C_IsValidCode_InvalidDay		2

#Const C_DaysInMonth [
	1 => 31,
	2 => 28, //< Leap years are checked 👀
	3 => 31,
	4 => 30,
	5 => 31,
	6 => 30,
	7 => 31,
	8 => 31,
	9 => 30,
	10 => 31,
	11 => 30,
	12 => 31
]

/**
 * Validates a date.
 * Years are unrestricted, months are between 1 and 12, days are between 1 and 28, 29, 30 or 31 according to the month and year.
 * Leap years are checked.
 * @param _Date The date to validate.
 * @return 0 if the validation succeded, other positive values otherwise. Error can be aknowledged with the C_IsValidCode_... constants.
 */
Integer Validate(M_Date _Date) {
	if (_Date.Month < 1 || _Date.Month > 12) {
		return C_IsValidCode_InvalidMonth;
	}

	declare Integer DaysInMonth = C_DaysInMonth[_Date.Month];
	if (_Date.Month == 2 && _Date.Year % 4 == 0) DaysInMonth = 29;
	if (_Date.Day < 1 && _Date.Day > DaysInMonth) {
		return C_IsValidCode_InvalidDay;
	}

	return C_IsValidCode_Success;
}

/**
 * Validates a date.
 * Years are unrestricted, months are between 1 and 12, days are between 1 and 28, 29, 30 or 31 according to the month and year.
 * Leap years are checked.
 * @param _Year The year of the date to validate.
 * @param _Month The month of the date to validate.
 * @param _Day The day of the date to validate.
 * @return 0 if the validation succeded, other positive values otherwise. Error can be aknowledged with the C_IsValidCode_... constants.
 */
Integer Validate(Integer _Year, Integer _Month, Integer _Day) {
	return Validate(M_Date{
		Year = _Year,
		Month = _Month,
		Day = _Day
	});
}
