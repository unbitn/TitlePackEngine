#Struct M_ResultOptions {
	Boolean Minify;
	Boolean SkipHeader;
	Boolean IndentWithSpaces;
	Integer IndentSpaceCount;
}
