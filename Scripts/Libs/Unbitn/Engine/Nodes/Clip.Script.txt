/**
 * Generated main script #Namespace#/Nodes/#NodeName#.Script.txt
 */
#Include "Libs/Unbitn/TextLib.Script.txt" as TL
#Include "Libs/Unbitn/XmlLib.Script.txt" as XL
#Include "Libs/Unbitn/Engine/Log/Log.Script.txt" as Log
#Include "Libs/Unbitn/Engine/EventManager/EventManager.Script.txt" as EventMgr
#Include "Libs/Unbitn/Engine/Renderer/Enums/EventType.Script.txt" as ERendererEventType
#Include "Libs/Unbitn/Engine/Renderer/Renderer.Script.txt" as Renderer

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeProperties.Script.txt" as NodeProperties
#Struct NodeProperties::M_NodeProperties as M_NodeProperties
#Struct NodeProperties::M_AttributeProperties as M_AttributeProperties

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeToProcessEventData.Script.txt" as NodeToProcessEventData
#Struct NodeToProcessEventData::M_NodeToProcessEventData as M_NodeToProcessEventData

#Const C_ScriptName "NodeClip"

#Const C_NodeProperties M_NodeProperties {
	Name = "clip",
	CanHaveChildren = True,
	Attributes = [
		"id" => M_AttributeProperties {
			Type = "Text"
		},
		"enabled" => M_AttributeProperties {
			Type = "Boolean",
			DefaultValue = "1"
		},
		"z-index" => M_AttributeProperties {
			Type = "Real",
			DefaultValue = "0"
		},
		"pos" => M_AttributeProperties {
			Type = "Vec2",
			DefaultValue = "0 0"
		},
		"hidden" => M_AttributeProperties {
			Type = "Boolean",
			DefaultValue = "0"
		},
		"origin" => M_AttributeProperties {
			Type = "Vec2",
			DefaultValue = "0 0"
		},
		"size" => M_AttributeProperties {
			Type = "Vec2",
			IsMandatory = True
		},
		"align" => M_AttributeProperties {
			Type = "Text",
			DefaultValue = "top left",
			PossibleTexts = [
				"top left", "top center", "top right",
				"center left", "center center", "center right",
				"bottom left", "bottom center", "bottom right"
			]
		}
	]
}

declare Text G_Subscription;

/**
 * Contains the node's core computing code
 * @param _EventData The caught event.
 */
Void Private_Process(M_NodeToProcessEventData _EventData) {
	Log::Verbose(C_ScriptName, "Processing node.");

	declare Boolean ClipEnabled = TL::ToBoolean(_EventData.Attributes["enabled"]);
	declare Vec2 ClipPos = TL::ToVec2(_EventData.Attributes["pos"]);
	declare Text Hidden = _EventData.Attributes["hidden"];
	declare Text ZIndex = _EventData.Attributes["z-index"];
	declare Text ClipSizeAttr;
	declare Text ClipOuterPosAttr;
	declare Text ClipInnerPosAttr;

	declare Text IdAttr;
	if (_EventData.Attributes.existskey("id")) {
		IdAttr = " id=\"" ^ _EventData.Attributes["id"] ^ "\"";
	}

	if (ClipEnabled) {
		declare Vec2 ClipSize = TL::ToVec2(_EventData.Attributes["size"]);
		ClipSizeAttr = " size=\"" ^ TL::ToText(ClipSize) ^ "\"";

		declare Vec2 ClipOrigin = TL::ToVec2(_EventData.Attributes["origin"]);
		declare Text ClipAlign = _EventData.Attributes["align"];
		declare TL::M_RegexMatchResult MatchResult = TL::RegexMatch("""^(\w+)\s+(\w+)$""", ClipAlign);
		declare Text ClipVAlign = MatchResult.Groups[0];
		declare Text ClipHAlign = MatchResult.Groups[1];

		switch (ClipVAlign) {
			case "center": ClipOrigin.Y += ClipSize.Y / 2.;
			case "bottom": ClipOrigin.Y += ClipSize.Y;
		}

		switch (ClipHAlign) {
			case "center": ClipOrigin.X -= ClipSize.X / 2.;
			case "right": ClipOrigin.X -= ClipSize.X;
		}
		
		ClipOuterPosAttr = " pos=\"" ^ TL::ToText(ClipOrigin + ClipPos) ^ "\"";
		ClipInnerPosAttr = " pos=\"" ^ TL::ToText(-ClipOrigin) ^ "\"";
	} else {
		ClipOuterPosAttr = " pos=\"" ^ TL::ToText(ClipPos) ^ "\"";
	}

	Renderer::Combine(_EventData, """
	<frame{{{IdAttr}}}{{{ClipOuterPosAttr}}} z-index="{{{ZIndex}}}"{{{ClipSizeAttr}}} hidden="{{{Hidden}}}">
		<frame{{{ClipInnerPosAttr}}}>
			<Children guids="{{{ TL::MLEncode(_EventData.Children.tojson()) }}}"/>
		</frame>
	</frame>
	""");
}

/**
 * Checks for NodeToProcess events.
 * Function to call in the main script's Loop label.
 */
Void Loop() {
	foreach (Event in EventMgr::GetSubscribedEvents(G_Subscription)) {
		declare M_NodeToProcessEventData EventData = NodeToProcessEventData::Deserialize(Event.Data);
		if (EventData.NodeName == C_NodeProperties.Name) {
			Private_Process(EventData);
			EventMgr::Consume(Event);
		}
	}
}

/**
 * Initializes the node and subscribes to the NodeToProcess event.
 * Send this function's output as argument of Renderer::RegisterNode().
 * @return An M_NodeProperties structure containing information about the node.
 */
M_NodeProperties Init() {
	G_Subscription = EventMgr::Subscribe(ERendererEventType::C_NodeToProcess);
	return C_NodeProperties;
}
