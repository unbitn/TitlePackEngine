#Include "Libs/Unbitn/TextLib.Script.txt" as TL
#Include "Libs/Unbitn/Engine/EventManager/EventManager.Script.txt" as EventMgr
#Include "Libs/Unbitn/Engine/Renderer/Renderer.Script.txt" as Renderer
#Include "Libs/Unbitn/Engine/Animations/Models/Animation.Script.txt" as Animation
#Include "Libs/Unbitn/Engine/Animations/Models/Animations.Script.txt" as Animations
#Include "Libs/Unbitn/Engine/Renderer/Enums/EventType.Script.txt" as ERendererEventType
#Include "Libs/Unbitn/Engine/Animations/Consts.Script.txt" as Consts
#Include "Libs/Unbitn/Engine/Animations/Enums/AnimationType.Script.txt" as AnimationType

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeToProcessEventData.Script.txt" as NodeToProcessEventData
#Struct NodeToProcessEventData::M_NodeToProcessEventData as M_NodeToProcessEventData

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeProperties.Script.txt" as NodeProperties
#Struct NodeProperties::M_NodeProperties as M_NodeProperties
#Struct NodeProperties::M_AttributeProperties as M_AttributeProperties

#Const C_NodeProperties	M_NodeProperties {
	Name = "animations",
	IsSingleton = True,
	IsPure = True
}

declare Text G_Subscription;

Void Private_Process(M_NodeToProcessEventData _EventData) {
	declare Text Maniascript = """
		#Include "MathLib" as ML
		#Include "ColorLib" as CL
		
		{{{ Animation::C_M_Animation }}}
		{{{ Animations::C_M_Animations }}}

		#Struct M_PreparedAnimation {
			CMlControl Control;
			Text Tag;
			Integer Delay;
			Integer Duration;
			CAnimManager::EAnimManagerEasing Easing;
		}
		
		#Const C_Animations_ClassAnimate	{{{ dump(Consts::C_ClassAnimate) }}}

		#Const C_Animations_Position			{{{ dump(AnimationType::C_Position) }}}
		#Const C_Animations_Size					{{{ dump(AnimationType::C_Size) }}}
		#Const C_Animations_Opacity				{{{ dump(AnimationType::C_Opacity) }}}
		#Const C_Animations_Scale					{{{ dump(AnimationType::C_Scale) }}}
		#Const C_Animations_Rotation			{{{ dump(AnimationType::C_Rotation) }}}
		#Const C_Animations_Color					{{{ dump(AnimationType::C_Color) }}}
		
		***Init***
		***
		declare M_PreparedAnimation[] Animations_StartAnims;
		declare M_PreparedAnimation[] Animations_EndAnims;
		declare Integer Animations_StartAnimationsEndTimestamp;
		declare Integer Animations_EndAnimationsEndTimestamp;

		Page.GetClassChildren(C_Animations_ClassAnimate, Page.MainFrame, True);
		foreach (ClassChild in Page.GetClassChildren_Result) {
			// ClassChild is a reference to an element in Page.GetClassChildren_Result.
			declare CMlControl AnimatedControl = ClassChild;

			if (AnimatedControl.DataAttributeExists("animations")) {
				declare Text Json = AnimatedControl.DataAttributeGet("animations");
				declare M_Animations Animations;
				assert(Animations.fromjson(Json), "The JSON unserialization failed");
				
				// Start animations.
				foreach (Animation in Animations.Start) {
					declare Text AnimationTag;

					switch (Animation.Type) {
						case C_Animations_Position: {
							AnimationTag = '''<control pos="<<<AnimatedControl.RelativePosition_V3.X>>> <<<AnimatedControl.RelativePosition_V3.Y>>>"/>''';
							AnimatedControl.RelativePosition_V3 = Animation.Position;
						}
						case C_Animations_Size: {
							AnimationTag = '''<control size="<<<AnimatedControl.Size.X>>> <<<AnimatedControl.Size.Y>>>"/>''';
							AnimatedControl.Size = Animation.Size;
						}
						case C_Animations_Opacity: {
							switchtype (AnimatedControl) {
								case CMlEntry: {
									declare CMlEntry AnimatedEntry = (AnimatedControl as CMlEntry);
									AnimationTag = '''<entry opacity="<<<AnimatedEntry.Opacity>>>"/>''';
									AnimatedEntry.Opacity = Animation.Opacity;
								}
								case CMlLabel: {
									declare CMlLabel AnimatedLabel = (AnimatedControl as CMlLabel);
									AnimationTag = '''<label opacity="<<<AnimatedLabel.Opacity>>>"/>''';
									AnimatedLabel.Opacity = Animation.Opacity;
								}
								case CMlQuad: {
									declare CMlQuad AnimatedQuad = (AnimatedControl as CMlQuad);
									AnimationTag = '''<quad opacity="<<<AnimatedQuad.Opacity>>>"/>''';
									AnimatedQuad.Opacity = Animation.Opacity;
								}
								case CMlTextEdit: {
									declare CMlTextEdit AnimatedTextEdit = (AnimatedControl as CMlTextEdit);
									AnimationTag = '''<textedit opacity="<<<AnimatedTextEdit.Opacity>>>"/>''';
									AnimatedTextEdit.Opacity = Animation.Opacity;
								}
							}
						}
						case C_Animations_Scale: {
							AnimationTag = '''<control scale="<<<AnimatedControl.RelativeScale>>>"/>''';
							AnimatedControl.RelativeScale = Animation.Scale;
						}
						case C_Animations_Rotation: {
							AnimationTag = '''<control rot="<<<AnimatedControl.RelativeRotation>>>"/>''';
							AnimatedControl.RelativeRotation = Animation.Rotation;
						}
						case C_Animations_Color: {
							switchtype (AnimatedControl) {
								case CMlLabel: {
									declare CMlLabel Label = (AnimatedControl as CMlLabel);
									AnimationTag = '''<label textcolor="<<<CL::RgbToHex(Label.TextColor)>>>"/>''';
									Label.TextColor = Animation.Color;
								}
								case CMlQuad: {
									declare CMlQuad Quad = (AnimatedControl as CMlQuad);
									AnimationTag = '''<quad bgcolor="<<<CL::RgbToHex(Quad.BgColor)>>>"/>''';
									Quad.BgColor = Animation.Color;
								}
							}
						}
					}

					Animations_StartAnims.add(M_PreparedAnimation {
						Control = AnimatedControl,
						Tag = AnimationTag,
						Delay = Animation.Delay,
						Duration = Animation.Duration,
						Easing = Animation.Easing
					});
				}

				// End animations.
				foreach (Animation in Animations.End) {
					declare Text AnimationTag;

					switch (Animation.Type) {
						case C_Animations_Position: {
							AnimationTag = '''<control pos="<<<Animation.Position.X>>> <<<Animation.Position.Y>>>"/>''';
						}
						case C_Animations_Size: {
							AnimationTag = '''<control size="<<<Animation.Size.X>>> <<<Animation.Size.Y>>>"/>''';
						}
						case C_Animations_Opacity: {
							switchtype (AnimatedControl) {
								case CMlEntry: {
									AnimationTag = '''<entry opacity="<<<Animation.Opacity>>>"/>''';
								}
								case CMlLabel: {
									AnimationTag = '''<label opacity="<<<Animation.Opacity>>>"/>''';
								}
								case CMlQuad: {
									AnimationTag = '''<quad opacity="<<<Animation.Opacity>>>"/>''';
								}
								case CMlTextEdit: {
									AnimationTag = '''<textedit opacity="<<<Animation.Opacity>>>"/>''';
								}
							}
						}
						case C_Animations_Scale: {
							AnimationTag = '''<control scale="<<<Animation.Scale>>>"/>''';
						}
						case C_Animations_Rotation: {
							AnimationTag = '''<control rot="<<<Animation.Rotation>>>"/>''';
						}
						case C_Animations_Color: {
							switchtype (AnimatedControl) {
								case CMlLabel: AnimationTag = '''<label textcolor="<<<Animation.Color>>>"/>''';
								case CMlQuad: AnimationTag = '''<quad bgcolor="<<<Animation.Color>>>"/>''';
							}
						}
					}

					Animations_EndAnims.add(M_PreparedAnimation {
						Control = AnimatedControl,
						Tag = AnimationTag,
						Delay = Animation.Delay,
						Duration = Animation.Duration,
						Easing = Animation.Easing
					});
				}
			}
		}
		***

		***Start***
		***
		declare Integer Animations_MaxStartAnimDuration = 0;
		foreach (Animation in Animations_StartAnims) {
			AnimMgr.Add(
				Animation.Control,
				Animation.Tag,
				Now + Animation.Delay,
				Animation.Duration,
				Animation.Easing
			);

			Animations_MaxStartAnimDuration = ML::Max(Animations_MaxStartAnimDuration, Animation.Delay + Animation.Duration);
		}
		
		Animations_StartAnimationsEndTimestamp = Now + Animations_MaxStartAnimDuration;
		***

		***LoopStop***
		***
		if (Animations_StartAnimationsEndTimestamp < Now) {
			if (Animations_EndAnimationsEndTimestamp == 0) {
				declare Integer Animations_MaxEndAnimDuration = 0;
				foreach (Animation in Animations_EndAnims) {
					AnimMgr.Add(
						Animation.Control,
						Animation.Tag,
						Now + Animation.Delay,
						Animation.Duration,
						Animation.Easing
					);

					Animations_MaxEndAnimDuration = ML::Max(Animations_MaxEndAnimDuration, Animation.Delay + Animation.Duration);
				}
				
				Animations_EndAnimationsEndTimestamp = Now + Animations_MaxEndAnimDuration;
			} else if (Animations_EndAnimationsEndTimestamp < Now) {
				ReadyToStop = True;
			}
		}
		***""";

	Renderer::AddManiascript(_EventData, Maniascript);
	Renderer::IgnoreAndResume(_EventData);
}

Void Loop() {
	foreach (Event in EventMgr::GetSubscribedEvents(G_Subscription)) {
		declare M_NodeToProcessEventData EventData = NodeToProcessEventData::Deserialize(Event.Data);

		if (C_NodeProperties.Name == EventData.NodeName) {
			Private_Process(EventData);
			EventMgr::Consume(Event);
		}
	}
}

M_NodeProperties Init() {
	G_Subscription = EventMgr::Subscribe(ERendererEventType::C_NodeToProcess);
	return C_NodeProperties;
}
