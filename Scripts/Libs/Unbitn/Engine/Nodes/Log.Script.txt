#Include "Libs/Unbitn/Engine/Log/Log.Script.txt" as Log
#Include "Libs/Unbitn/Engine/Log/Models/LogLine.Script.txt" as LogLine
#Include "Libs/Unbitn/Engine/Log/Enums/LogType.Script.txt" as LogType
#Include "Libs/Unbitn/TextLib.Script.txt" as TL
#Include "Libs/Unbitn/Engine/EventManager/EventManager.Script.txt" as EventMgr
#Include "Libs/Unbitn/Engine/Renderer/Renderer.Script.txt" as Renderer
#Include "Libs/Unbitn/Engine/Renderer/Enums/EventType.Script.txt" as ERendererEventType

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeProperties.Script.txt" as NodeProperties
#Struct NodeProperties::M_NodeProperties as M_NodeProperties

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeToProcessEventData.Script.txt" as NodeToProcessEventData
#Struct NodeToProcessEventData::M_NodeToProcessEventData as M_NodeToProcessEventData

#Const C_ScriptName "NodeLog"

#Const C_NodeProperties	M_NodeProperties {
	Name = "log",
	CanHaveChildren = False,
	IsSingleton			= True
}

declare Text G_Subscription;

Void Private_Process(M_NodeToProcessEventData _EventData) {
	Log::Verbose(C_ScriptName, "Processing node.");
	declare Text Manialink = """
		<frame>
			<Env/>
			<Maniascript><![CDATA[
				#Include "TextLib" as TL

				{{{ LogLine::C_M_LogLine }}}

				#Const C_Log_ScriptName "Log"

				#Const C_Log_Log						{{{ dump(Log::C_Log) }}}
				#Const C_Log_Warn						{{{ dump(Log::C_Warn) }}}
				#Const C_Log_Error					{{{ dump(Log::C_Error) }}}

				#Const C_Log_LogType_Log		{{{ dump(LogType::C_Log) }}}
				#Const C_Log_LogType_Warn		{{{ dump(LogType::C_Warn) }}}
				#Const C_Log_LogType_Error	{{{ dump(LogType::C_Error) }}}
				
				declare Boolean G_Log_LogWarning;
				
				Void Log_Private_WriteLine(Integer _Time, Text _Type, Text _Script, Text _Message) {
					log(TL::GetTranslatedText(TL::Compose(
						"%1 | %2 | %3 | %4",
						_Type, ""^_Time, _Script, _Message
					)));
				}
				
				Void Log_Private_SetLogWarning(Boolean _WarningSent) {
					declare Boolean Unbitn_TPEngine_Log_LogWarning as LogWarning for LoadedTitle;
					LogWarning = _WarningSent;
				}
				
				Boolean Log_Private_IsLogWarningSent() {
					declare Boolean Unbitn_TPEngine_Log_LogWarning as LogWarning for LoadedTitle;
					return LogWarning;
				}
				
				Void Log_Private_SetWarnWarning(Boolean _WarningSent) {
					declare Boolean Unbitn_TPEngine_Log_WarnWarning as WarnWarning for LoadedTitle;
					WarnWarning = _WarningSent;
				}
				
				Boolean Log_Private_IsWarnWarningSent() {
					declare Boolean Unbitn_TPEngine_Log_WarnWarning as WarnWarning for LoadedTitle;
					return WarnWarning;
				}
				
				Void Log_Private_SetErrorWarning(Boolean _WarningSent) {
					declare Boolean Unbitn_TPEngine_Log_ErrorWarning as ErrorWarning for LoadedTitle;
					ErrorWarning = _WarningSent;
				}
				
				Boolean Log_Private_IsErrorWarningSent() {
					declare Boolean Unbitn_TPEngine_Log_ErrorWarning as ErrorWarning for LoadedTitle;
					return ErrorWarning;
				}

				/**
					* Prints out a line in the console with the given string.
					* @param _Script The script name.
					* @param _Message The string to write.
					*/
				Void Log_Log(Text _Script, Text _Message) {
					declare Text Env = Env_Get();
					declare Boolean Unbitn_TPEngine_Log_KeepLogs as KeepLogs for LoadedTitle;

					if (KeepLogs) {
						declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
						History.addfirst(M_LogLine {
							Time = Now,
							Type = C_Log_LogType_Log,
							Script = _Script,
							Message = _Message
						});
					}
				
					if (Env == C_Env_Development || Env == C_Env_Test) {
						Log_Private_WriteLine(Now, C_Log_Log, _Script, _Message);
					} else if (!Log_Private_IsLogWarningSent()) {
						Log_Private_SetLogWarning(True);
						Log_Private_WriteLine(Now, C_Log_Log, C_Log_ScriptName, '''Logging is disabled in the <<< dump(Env) >>> environment.''');
					}
				}
				
				/**
					* Prints out a line in the console with the given string if the verbose mode is enabled.
					* @param _Script The script name.
					* @param _Message The string to write.
					*/
				Void Log_Verbose(Text _Script, Text _Message) {
					declare Boolean Unbitn_TPEngine_Log_VerboseState as VerboseState for LoadedTitle;
					if (VerboseState) Log_Log(_Script, _Message);
				}
				
				/**
					* Prints out a warning line in the console with the given string.
					* @param _Script The script name.
					* @param _Message The warning string to write.
					*/
				Void Log_Warn(Text _Script, Text _Message) {
					declare Text Env = Env_Get();
					declare Boolean Unbitn_TPEngine_Log_KeepWarnings as KeepWarnings for LoadedTitle;

					if (KeepWarnings) {
						declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
						History.addfirst(M_LogLine {
							Time = Now,
							Type = C_Log_LogType_Warn,
							Script = _Script,
							Message = _Message
						});
					}
				
					if (Env == C_Env_Development || Env == C_Env_Test) {
						Log_Private_WriteLine(Now, C_Log_Warn, _Script, _Message);
					} else if (!Log_Private_IsWarnWarningSent()) {
						Log_Private_SetWarnWarning(True);
						Log_Private_WriteLine(Now, C_Log_Log, C_Log_ScriptName, '''Warning is disabled in the <<< dump(Env) >>> environment.''');
					}
				}
				
				/**
					* Writes an error line in the console with the given string.
					* @param _Script The script name.
					* @param _Message The error string to write.
					*/
				Void Log_Error(Text _Script, Text _Message) {
					declare Text Env = Env_Get();
					declare Boolean Unbitn_TPEngine_Log_KeepErrors as KeepErrors for LoadedTitle;

					if (KeepErrors) {
						declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
						History.addfirst(M_LogLine {
							Time = Now,
							Type = C_Log_LogType_Error,
							Script = _Script,
							Message = _Message
						});
					}
				
					if (Env == C_Env_Development || Env == C_Env_Test) {
						Log_Private_WriteLine(Now, C_Log_Error, _Script, _Message);
					} else if (!Log_Private_IsErrorWarningSent()) {
						Log_Private_SetErrorWarning(True);
						Log_Private_WriteLine(Now, C_Log_Log, C_Log_ScriptName, '''Erroring is disabled in the <<< dump(Env) >>> environment.''');
					}
				}
			]]></Maniascript>
		</frame>
	""";

	Renderer::Combine(_EventData, Manialink);
}

Void Loop() {
	foreach (Event in EventMgr::GetSubscribedEvents(G_Subscription)) {
		declare M_NodeToProcessEventData EventData = NodeToProcessEventData::Deserialize(Event.Data);

		if (C_NodeProperties.Name == EventData.NodeName) {
			Private_Process(EventData);
			EventMgr::Consume(Event);
		}
	}
}

M_NodeProperties Init() {
	G_Subscription = EventMgr::Subscribe(ERendererEventType::C_NodeToProcess);
	return C_NodeProperties;
}
