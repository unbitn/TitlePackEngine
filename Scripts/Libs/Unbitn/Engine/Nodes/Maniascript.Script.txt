#Include "Libs/Unbitn/TextLib.Script.txt" as TL
#Include "Libs/Unbitn/Engine/Log/Log.Script.txt" as Log
#Include "Libs/Unbitn/Engine/EventManager/EventManager.Script.txt" as EventMgr
#Include "Libs/Unbitn/Engine/Renderer/Enums/EventType.Script.txt" as ERendererEventType
#Include "Libs/Unbitn/Engine/Renderer/Renderer.Script.txt" as Renderer

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeProperties.Script.txt" as NodeProperties
#Struct NodeProperties::M_NodeProperties as M_NodeProperties

#Include "Libs/Unbitn/Engine/Renderer/Models/NodeToProcessEventData.Script.txt" as NodeToProcessEventData
#Struct NodeToProcessEventData::M_NodeToProcessEventData as M_NodeToProcessEventData

#Const C_ScriptName "NodeManiascript"

#Const C_NodeProperties M_NodeProperties {
	Name = "maniascript"
}

declare Text G_Subscription;

Void Private_Process(M_NodeToProcessEventData _EventData) {
	Log::Verbose(C_ScriptName, "Processing node.");
	Renderer::AddManiascript(_EventData, _EventData.Content);
	Renderer::IgnoreAndResume(_EventData);
}

Void Loop() {
	foreach (Event in EventMgr::GetSubscribedEvents(G_Subscription)) {
		declare M_NodeToProcessEventData EventData = NodeToProcessEventData::Deserialize(Event.Data);

		if (C_NodeProperties.Name == EventData.NodeName) {
			Private_Process(EventData);
			EventMgr::Consume(Event);
		}
	}
}

M_NodeProperties Init() {
	G_Subscription = EventMgr::Subscribe(ERendererEventType::C_NodeToProcess);
	return C_NodeProperties;
}