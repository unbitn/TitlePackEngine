#Include "Libs/Unbitn/TextLib.Script.txt" as TL
#Include "Libs/Unbitn/Engine/Log/Log.Script.txt" as Log
#Include "Libs/Unbitn/Engine/EventManager/EventManager.Script.txt" as EventMgr
#Include "Libs/Unbitn/Engine/LayersManager/LayersManager.Script.txt" as LayersMgr
#Include "Libs/Unbitn/Engine/LayersManager/Enums/DisplayMethod.Script.txt" as EDisplayMethod
#Include "Libs/Unbitn/Engine/Enums/Stack.Script.txt" as EStack
#Include "Libs/Unbitn/Engine/Navigation/Consts.Script.txt" as NavigationConsts
#Include "Libs/Unbitn/Engine/Console/Enums/EventType.Script.txt" as EEventType

#Include "Libs/Unbitn/Engine/EventManager/Models/Event.Script.txt" as Event
#Struct Event::M_Event as M_Event

#Include "Libs/Unbitn/Engine/ResponseHandler/Models/View.Script.txt" as View
#Struct View::M_View as M_View

#Include "Libs/Unbitn/Engine/ResponseHandler/Models/Response.Script.txt" as Response
#Struct Response::M_Response as M_Response

#Include "Libs/Unbitn/Engine/Console/Models/CommandDispatchedEventData.Script.txt" as CommandDispatchedEventData
#Struct CommandDispatchedEventData::M_CommandDispatchedEventData as M_CommandDispatchedEventData

#Const C_ScriptName		"Console"
#Const C_CommandName	"console"

#Const C_Id "Console"

#Const C_DispatchCommand "DispatchCommand"

#Const C_CustomEventType	"Console_VisibilityChanged"
#Const C_CtrlLeft 65

declare Text G_Subscription;

Boolean Private_IsFilterTimeActive() {
	declare Integer Unbitn_TPEngine_Console_FilterTimeWindowStart as TimeWindowStart for LoadedTitle;
	declare Integer Unbitn_TPEngine_Console_FilterTimeWindowEnd as TimeWindowEnd for LoadedTitle;
	return (TimeWindowStart != 0 || TimeWindowEnd != 0);
}

Boolean Private_IsFilterTypeActive() {
	declare Boolean Unbitn_TPEngine_Console_FilterTypeLogDisabled as TypeLogDisabled for LoadedTitle;
	declare Boolean Unbitn_TPEngine_Console_FilterTypeWarnDisabled as TypeWarnDisabled for LoadedTitle;
	declare Boolean Unbitn_TPEngine_Console_FilterTypeErrorDisabled as TypeErrorDisabled for LoadedTitle;
	return TypeLogDisabled || TypeWarnDisabled || TypeErrorDisabled;
}

Boolean Private_IsFilterScriptActive() {
	declare Text Unbitn_TPEngine_Console_FilterScripts as FilterScripts for LoadedTitle;
	return (FilterScripts != "");
}

Boolean Private_IsFilterMessageActive() {
	declare Text Unbitn_TPEngine_Console_FilterMessagePattern as MessagePattern for LoadedTitle;
	return MessagePattern != "";
}

Void Toggle() {
	declare Boolean Unbitn_TPEngine_Console_Visibility as Visibility for LoadedTitle;
	Visibility = !Visibility;

	LayersMgr::SendLayerEvent(EStack::C_EngineConsole, C_Id, C_CustomEventType, [""^Visibility]);
}

M_Response Get() {
	declare Text View = """
<View z-index="3.4e+38">
	<Log/>
	<MlLib/>

	<framemodel id="fm-log-line">
		<frame pos="0 -0.5">
			<label id="l-time" size="20" textsize="1" maxline="1" appendellipsis="1"/>
			<label id="l-type" pos="20" size="18" textsize="1" maxline="1" appendellipsis="1"/>
			<label id="l-script" pos="40" size="38" textsize="1" maxline="1" appendellipsis="1"/>
			<label id="l-message" pos="80" size="228" textsize="1" maxline="1" appendellipsis="1"/>
			<label id="l-lines" pos="309" halign="right" textsize="1" opacity="0.8"/>
			<label id="l-more" class="l-btn" pos="312 -1.5" size="4 4" halign="center" valign="center2" textsize="1" text="" scriptevents="1" focusareacolor1="0000" focusareacolor2="0000" opacity="0.8"/>
			<label id="l-copy" class="l-btn" pos="316 -1.5" size="4 4" halign="center" valign="center2" textsize="1" text="" scriptevents="1" focusareacolor1="0000" focusareacolor2="0000" opacity="0.8"/>
		</frame>
		<quad id="q-line" pos="0 -3.9" size="320 0.1" bgcolor="000" opacity="0.5"/>
		<quad id="q-bg" size="320 4" bgcolor="000" opacity="0.5" scriptevents="1"/>
	</framemodel>

	<frame id="f-more" pos="-150 80" hidden="1">
		<quad size="300 0.5" bgcolor="000"/>
		<label id="l-more-close" class="l-btn" pos="297 -2.5" halign="center" valign="center" text="" scriptevents="1"/>

		<label id="l-more-info" pos="5 -5" textsize="1"/>
		
		<textedit id="t-more" pos="15 -10" size="280 85" textsize="1" showlinenumbers="1" style="StyleTextScriptEditor" textcolor="fff"/>
		<quad pos="5 -10" size="10 85" bgcolor="000"/>

		<quad size="300 100" bgcolor="000" opacity="0.75" scriptevents="1"/>
	</frame>
	
	<frame id="f-console" pos="-160 -90">
		<quad size="320 0.5" bgcolor="000"/>

		<frame id="f-filters" pos="0 -1">
			<!-- Time filter -->
			<frame>
				<label id="l-filter-time-active" pos="1 -1" size="3 3" text="" textsize="0.5" textcolor="0f8" textemboss="1" hidden="{{{!Private_IsFilterTimeActive()}}}"/>
				<label id="l-filter-time" class="l-btn"
					pos="1.5 -1.5" size="3 3" halign="center" valign="center2"
					textsize="0.5" text="" focusareacolor1="0000" focusareacolor2="0000" opacity="0.8"
					scriptevents="1"/>
				<label pos="04" textsize="1" text="Time"/>
				<frame id="f-filter-time" pos="0 1" hidden="1">
					<label pos="2 -6" size="10 3" text="Start" textsize="1"/>
					<label id="l-filter-time-start-reset" class="l-btn" pos="60.5 -7.5" size="3 3" halign="center" valign="center2" text="" textsize="1" focusareacolor1="0000" focusareacolor2="0000" scriptevents="1"/>
					<entry id="e-filter-time-start" pos="14 -6" size="48 3" textsize="1" focusareacolor1="fff1" focusareacolor2="fff2" scriptevents="1"/>
					
					<label pos="2 -11" size="10 3" text="End" textsize="1"/>
					<label id="l-filter-time-end-reset" class="l-btn" pos="60.5 -12.5" size="3 3" halign="center" valign="center2" text="" textsize="1" focusareacolor1="0000" focusareacolor2="0000" scriptevents="1"/>
					<entry id="e-filter-time-end" pos="14 -11" size="48 3" textsize="1" focusareacolor1="fff1" focusareacolor2="fff2" scriptevents="1"/>

					<quad pos="0 -4" size="64 12" scriptevents="1"/>
					<quad size="64 16" bgcolor="000" opacity="0.95"/>
				</frame>
			</frame>

			<!-- Type filter -->
			<frame pos="20">
				<label id="l-filter-type-active" pos="1 -1" size="3 3" text="" textsize="0.5" textcolor="0f8" textemboss="1" hidden="{{{!Private_IsFilterTypeActive()}}}"/>
				<label id="l-filter-type" class="l-btn"
					pos="1.5 -1.5" size="3 3" halign="center" valign="center2"
					textsize="0.5" text="" focusareacolor1="0000" focusareacolor2="0000" opacity="0.8"
					scriptevents="1"/>
				<label pos="4" textsize="1" text="Type"/>
				<frame id="f-filter-type" pos="0 1" hidden="1">
					<Checkbox id="c-filter-type-log" class="{{{ NavigationConsts::C_ClassNav }}}" pos="2 -6" size="18.67 3"
						text="Log" textsize="1" focuscolor="fff" checked="1"/>
					<Checkbox id="c-filter-type-warn" class="{{{ NavigationConsts::C_ClassNav }}}" pos="22.665 -6" size="18.67 3"
						text="Warn" textsize="1" focuscolor="fff" checked="1"/>
					<Checkbox id="c-filter-type-error" class="{{{ NavigationConsts::C_ClassNav }}}" pos="43.33 -6" size="18.67 3"
						text="Error" textsize="1" focuscolor="fff" checked="1"/>
					
					<quad pos="0 -4" size="64 7" scriptevents="1"/>
					<quad size="64 11" bgcolor="000" opacity="0.95"/>
				</frame>
			</frame>

			<!-- Script filter -->
			<frame pos="40">
				<label id="l-filter-script-active" pos="1 -1" size="3 3" text="" textsize="0.5" textcolor="0f8" textemboss="1" hidden="{{{!Private_IsFilterScriptActive()}}}"/>
				<label id="l-filter-script" class="l-btn"
					pos="1.5 -1.5" size="3 3" halign="center" valign="center2"
					textsize="0.5" text="" focusareacolor1="0000" focusareacolor2="0000" opacity="0.8"
					scriptevents="1"/>
				<label pos="4" textsize="1" text="Script"/>
				<frame id="f-filter-script" pos="0 1" hidden="1">
					<label pos="2 -6" size="10 3" text="Scripts" textsize="1"/>
					<label id="l-filter-scripts-reset" class="l-btn" pos="60.5 -7.5" size="3 3" halign="center" valign="center2" text="" textsize="1" focusareacolor1="0000" focusareacolor2="0000" scriptevents="1"/>
					<entry id="e-filter-scripts" pos="14 -6" size="48 3" textsize="1" focusareacolor1="fff1" focusareacolor2="fff2" scriptevents="1"/>
					
					<quad pos="0 -4" size="64 7" scriptevents="1"/>
					<quad size="64 11" bgcolor="000" opacity="0.95"/>
				</frame>
			</frame>

			<!-- Message filter -->
			<frame pos="80">
				<label id="l-filter-message-active" pos="1 -1" size="3 3" text="" textsize="0.5" textcolor="0f8" textemboss="1" hidden="{{{!Private_IsFilterMessageActive()}}}"/>
				<label id="l-filter-message" class="l-btn"
					pos="1.5 -1.5" size="3 3" halign="center" valign="center2"
					textsize="0.5" text="" focusareacolor1="0000" focusareacolor2="0000" opacity="0.8"
					scriptevents="1"/>
				<label pos="4" textsize="1" text="Message"/>
				<frame id="f-filter-message" pos="0 1" hidden="1">
					<label pos="2 -6" size="10 3" text="RegEx" textsize="1"/>
					<label id="l-filter-message-regex-reset" class="l-btn" pos="60.5 -7.5" size="3 3" halign="center" valign="center2" text="" textsize="1" focusareacolor1="0000" focusareacolor2="0000" scriptevents="1"/>
					<entry id="e-filter-message-regex" pos="14 -6" size="48 3" textsize="1" focusareacolor1="fff1" focusareacolor2="fff2" scriptevents="1"/>

					<quad pos="0 -4" size="64 7" scriptevents="1"/>
					<quad size="64 11" bgcolor="000" opacity="0.95"/>
				</frame>
			</frame>


			<label id="l-scroll-lock" class="l-btn"
				pos="316.5 -1.5" size="3 3" halign="center" valign="center2"
				textsize="0.5" text="" focusareacolor1="0000" focusareacolor2="0000" opacity="0.8"
				scriptevents="1"/>
		</frame>

		<frame id="f-log-lines" pos="0 -4">
			<frameinstance modelid="fm-log-line" pos="0 0"/>
			<frameinstance modelid="fm-log-line" pos="0 -4"/>
			<frameinstance modelid="fm-log-line" pos="0 -8"/>
			<frameinstance modelid="fm-log-line" pos="0 -12"/>
			<frameinstance modelid="fm-log-line" pos="0 -16"/>
			<frameinstance modelid="fm-log-line" pos="0 -20"/>
			<frameinstance modelid="fm-log-line" pos="0 -24"/>
			<frameinstance modelid="fm-log-line" pos="0 -28"/>
			<frameinstance modelid="fm-log-line" pos="0 -32"/>
			<frameinstance modelid="fm-log-line" pos="0 -36"/>
			<frameinstance modelid="fm-log-line" pos="0 -40"/>
			<frameinstance modelid="fm-log-line" pos="0 -44"/>
			<frameinstance modelid="fm-log-line" pos="0 -48"/>
		</frame>

		<frame id="f-cmd" pos="0 -56">
			<entry id="e-cmd" pos="80" size="240 4" textsize="1" focusareacolor1="0000" focusareacolor2="0000" scriptevents="1"/>
			<quad size="320 4" bgcolor="000" opacity="0.95"/>
		</frame>
		
		<quad id="q-console-bg" size="320 60" bgcolor="000" opacity="0.75" scriptevents="1"/>
	</frame>

	<Maniascript><![CDATA[
		#Include "MathLib" as ML

		#Const C_ScriptName "Console"

		#Const C_DispatchCommand	{{{ dump(C_DispatchCommand) }}}

		#Const C_CustomEventType	{{{ dump(C_CustomEventType) }}}
		
		#Const C_CtrlLeft 65
		#Const C_CtrlRight 108

		#Const C_MinInt -2147483648
		#Const C_MaxInt 2147483647
		
		#Const C_ScrollLockIcons [
			False	=> "",
			True	=> ""
		]
		
		#Const C_AnimationDuration			250

		#Const C_LabelScale							1.
		#Const C_LabelScaleClick				0.75

		#Const C_LabelOpacity						0.8
		#Const C_LabelOpacityClick			1.

		#Const C_BgQuadOpacity					0.5
		#Const C_BgQuadOpacityFocus			0.6
		
		declare CMlFrame[] G_LogLines;
		declare Text G_SelectedScript;

		declare Boolean G_ScrollLock;

		***Init***
		***
		declare Text[] CommandHistory;
		declare Integer CommandHistoryIndex = -1;
		
		declare Boolean SavedVisibility = IsVisible();
		declare Integer StartingIndex = 0;
		declare Integer SavedHistoryCount = -1;

		declare Boolean Compute;
		declare Boolean Draw;
		
		declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
		declare M_LogLine[] FilteredHistory;

		declare CMlFrame MoreFrame = GetFrameById("f-more");
		declare CMlTextEdit MoreTextEdit = GetTextEditById("t-more");
		declare CMlLabel MoreInfoLabel = GetLabelById("l-more-info");

		declare CMlFrame ConsoleFrame = GetFrameById("f-console");
		declare CMlQuad ConsoleBgQuad = GetQuadById("q-console-bg");
		declare CMlFrame LinesFrame = GetFrameById("f-log-lines");

		declare CMlFrame FiltersFrame = GetFrameById("f-filters");
		
		declare CMlFrame FilterTimeFrame = GetFrameById("f-filter-time");
		declare CMlFrame FilterTypeFrame = GetFrameById("f-filter-type");
		declare CMlFrame FilterScriptFrame = GetFrameById("f-filter-script");
		declare CMlFrame FilterMessageFrame = GetFrameById("f-filter-message");

		declare CMlLabel FilterTimeActiveLabel = GetLabelById(FiltersFrame, "l-filter-time-active");
		declare CMlLabel FilterTypeActiveLabel = GetLabelById(FiltersFrame, "l-filter-type-active");
		declare CMlLabel FilterScriptsActiveLabel = GetLabelById(FiltersFrame, "l-filter-script-active");
		declare CMlLabel FilterMessageActiveLabel = GetLabelById(FiltersFrame, "l-filter-message-active");

		declare CMlEntry FilterTimeWindowStartEntry = GetEntryById(FilterTimeFrame, "e-filter-time-start");
		declare CMlEntry FilterTimeWindowEndEntry = GetEntryById(FilterTimeFrame, "e-filter-time-end");
		declare CMlEntry FilterScriptsEntry = GetEntryById(FilterScriptFrame, "e-filter-scripts");
		declare CMlEntry FilterMessageRegexEntry = GetEntryById(FilterMessageFrame, "e-filter-message-regex");

		declare CMlEntry CommandEntry <=> GetEntryById("e-cmd");
		
		declare Boolean SavedCommandEntryFocus = CommandEntry.IsFocused;

		foreach (LogLine in LinesFrame.Controls) {
			G_LogLines.add((LogLine as CMlFrame));
		}
		***

		***Start***
		***
		G_SelectedScript = "EventManager";
		
		if (SavedVisibility) {
			ConsoleFrame.RelativePosition_V3.Y = -30.;
			StartOverridingEvents();
		}

		Checkbox_SetChecked(GetControlById(FilterTypeFrame, "c-filter-type-log"), !IsFilterTypeLogDisabled());
		Checkbox_SetChecked(GetControlById(FilterTypeFrame, "c-filter-type-warn"), !IsFilterTypeWarnDisabled());
		Checkbox_SetChecked(GetControlById(FilterTypeFrame, "c-filter-type-error"), !IsFilterTypeErrorDisabled());

		GetEntryById(FilterTimeFrame, "e-filter-time-start").Value = TL::ToText(GetFilterTimeWindowStart());
		GetEntryById(FilterTimeFrame, "e-filter-time-end").Value = TL::ToText(GetFilterTimeWindowEnd());
		
		GetEntryById(FilterScriptFrame, "e-filter-scripts").Value = GetFilterScripts();
		GetEntryById(FilterMessageFrame, "e-filter-message-regex").Value = GetFilterMessagePattern();

		G_ScrollLock = False;
		***
		
		***LoopActive***
		***
		if (SavedVisibility) {
			if (SavedHistoryCount != History.count) {
				SavedHistoryCount = History.count;
				Compute = True;
				Draw = True;
			}

			if (Compute) {
				Compute = False;
				declare Integer CountBefore = FilteredHistory.count;
				FilteredHistory = Filter(History);
				if (!G_ScrollLock) {
					StartingIndex = 0;
				} else {
					StartingIndex += FilteredHistory.count - CountBefore;
				}
				Draw = True;
			}

			if (Draw) {
				Draw = False;

				declare Integer HistoryIndex = StartingIndex;
				declare Integer LogLineIndex = 0;
				
				while (G_LogLines.existskey(G_LogLines.count - 1 - LogLineIndex) && FilteredHistory.existskey(HistoryIndex)) {
					declare CMlFrame LogLineFrame = G_LogLines[G_LogLines.count - 1 - LogLineIndex];
					declare Integer FilteredHistoryRefIndex for LogLineFrame;

					declare CMlLabel TimeLabel = GetTimeLabel(LogLineFrame);
					declare CMlLabel ScriptLabel = GetScriptLabel(LogLineFrame);
					declare CMlLabel TypeLabel = GetTypeLabel(LogLineFrame);
					declare CMlLabel MessageLabel = GetMessageLabel(LogLineFrame);
					declare CMlLabel LinesLabel = GetLinesLabel(LogLineFrame);
					declare CMlLabel CopyLabel = GetCopyLabel(LogLineFrame);
					declare CMlLabel MoreLabel = GetMoreLabel(LogLineFrame);
					declare CMlQuad LineQuad = GetLineQuad(LogLineFrame);
					declare CMlQuad BgQuad = GetBgQuad(LogLineFrame);
					
					declare Text TimeAsText = FormatTime(FilteredHistory[HistoryIndex].Time);
					declare Vec3 TextColor = TypeToTextColor(FilteredHistory[HistoryIndex].Type);
					declare Vec3 BgColor = TypeToBgColor(FilteredHistory[HistoryIndex].Type);
					declare Integer LinesCount = CountOccurrences(FilteredHistory[HistoryIndex].Message, "\n");

					FilteredHistoryRefIndex = HistoryIndex;
					
					TimeLabel.SetText(TimeAsText);
					TimeLabel.TextColor = TextColor;
					ScriptLabel.SetText(FilteredHistory[HistoryIndex].Script);
					ScriptLabel.TextColor = TextColor;
					TypeLabel.SetText(TypeToText(FilteredHistory[HistoryIndex].Type));
					TypeLabel.TextColor = TextColor;
					MessageLabel.SetText(FilteredHistory[HistoryIndex].Message);
					MessageLabel.TextColor = TextColor;
					if (LinesCount > 1) {
						LinesLabel.Value = LinesCount ^ " lines";
						LinesLabel.TextColor = TextColor;
						LinesLabel.Show();
					} else {
						LinesLabel.Hide();
					}
					CopyLabel.TextColor = TextColor;
					CopyLabel.Show();
					MoreLabel.TextColor = TextColor;
					MoreLabel.Visible = TL::Find("\n", FilteredHistory[HistoryIndex].Message, True, True);
					LineQuad.BgColor = BgColor;
					BgQuad.BgColor = BgColor;
					
					HistoryIndex += 1;
					LogLineIndex += 1;
				}

				while (G_LogLines.existskey(G_LogLines.count - 1 - LogLineIndex)) {
					declare CMlFrame LogLineFrame = G_LogLines[G_LogLines.count - 1 - LogLineIndex];
					declare Integer FilteredHistoryRefIndex for LogLineFrame;

					FilteredHistoryRefIndex = -1;

					GetTimeLabel(LogLineFrame).SetText("");
					GetScriptLabel(LogLineFrame).SetText("");
					GetTypeLabel(LogLineFrame).SetText("");
					GetMessageLabel(LogLineFrame).SetText("");
					GetLinesLabel(LogLineFrame).Hide();
					GetCopyLabel(LogLineFrame).Hide();
					GetMoreLabel(LogLineFrame).Hide();
					GetLineQuad(LogLineFrame).BgColor = <0., 0., 0.>;
					GetBgQuad(LogLineFrame).BgColor = <0., 0., 0.>;
					LogLineIndex += 1;
				}
			}

			foreach (Event in Checkbox_GetPendingEvents()) {
				switch (Event.Type) {
					case C_Checkbox_EventType_CheckChanged: {
						switch (Event.CheckboxControlId) {
							case "c-filter-type-log": {
								SetFilterTypeLogDisabled(!Event.Checked);
								FilterTypeActiveLabel.Visible = !Event.Checked || IsFilterTypeWarnDisabled() || IsFilterTypeErrorDisabled();
								Compute = True;
							}
							case "c-filter-type-warn": {
								SetFilterTypeWarnDisabled(!Event.Checked);
								FilterTypeActiveLabel.Visible = IsFilterTypeLogDisabled() || !Event.Checked || IsFilterTypeErrorDisabled();
								Compute = True;
							}
							case "c-filter-type-error": {
								SetFilterTypeErrorDisabled(!Event.Checked);
								FilterTypeActiveLabel.Visible = IsFilterTypeLogDisabled() || IsFilterTypeWarnDisabled() || !Event.Checked;
								Compute = True;
							}
						}
					}
				}
			}
			
			foreach (Event in Input.PendingEvents) {
				if (Event.Type == CInputEvent::EType::PadButtonPress) {
					switch (Event.Button) {
						case CInputEvent::EButton::A: {
							if (SavedCommandEntryFocus) {
								// Trigger submit.
								ConsoleBgQuad.Focus();
								declare Text Value = CommandEntry.Value;
								if (Value != "") {
									CommandHistory.addfirst(Value);
									CommandHistoryIndex = -1;
									declare Text[] CommandElements = TL::RegexFind('''("[^"]*"|\S+)''', Value, "g");
									SendCustomEvent(C_DispatchCommand, CommandElements);
									CommandEntry.Value = "";
									CommandEntry.StartEdition();
								}
							}
						}
						case CInputEvent::EButton::Up: {
							if (SavedCommandEntryFocus) {
								// Previous command.
								declare Integer SavedIndex = CommandHistoryIndex;
								CommandHistoryIndex = ML::Min(CommandHistory.count - 1, CommandHistoryIndex + 1);
								if (SavedIndex != CommandHistoryIndex) {
									ConsoleBgQuad.Focus();
									CommandEntry.Value = CommandHistory[CommandHistoryIndex];
									CommandEntry.StartEdition();
								}
							}
						}
						case CInputEvent::EButton::Down: {
							if (SavedCommandEntryFocus) {
								// Next command.
								declare Integer SavedIndex = CommandHistoryIndex;
								CommandHistoryIndex = ML::Max(CommandHistoryIndex - 1, -1);
								if (SavedIndex != CommandHistoryIndex) {
									ConsoleBgQuad.Focus();
									if (CommandHistoryIndex == -1) {
										CommandEntry.Value = "";
									} else {
										CommandEntry.Value = CommandHistory[CommandHistoryIndex];
									}
								}
								CommandEntry.StartEdition();
							}
						}
						case CInputEvent::EButton::L1: if (!MoreTextEdit.IsFocused && !CommandEntry.IsFocused) {
							// Previous page.
							declare Integer SavedIndex = StartingIndex;
							StartingIndex = ML::Min(ML::Max(FilteredHistory.count - G_LogLines.count, 0), StartingIndex + G_LogLines.count);
							if (SavedIndex == StartingIndex) {
								LinesFrame.ScrollBumpTop(); //< not working
							} else {
								Draw = True;
							}
						}
						case CInputEvent::EButton::R1: if (!MoreTextEdit.IsFocused && !CommandEntry.IsFocused) {
							// Next page.
							declare Integer SavedIndex = StartingIndex;
							StartingIndex = ML::Max(StartingIndex - G_LogLines.count, 0);
							if (SavedIndex == StartingIndex) {
								LinesFrame.ScrollBumpBottom(); //< not working
							} else {
								Draw = True;
							}
						}
						case CInputEvent::EButton::X: if (!MoreTextEdit.IsFocused && !CommandEntry.IsFocused) {
							// Origin.
							StartingIndex = ML::Max(FilteredHistory.count - G_LogLines.count, 0);
							Draw = True;
						}
						case CInputEvent::EButton::Y: if (!MoreTextEdit.IsFocused && !CommandEntry.IsFocused) {
							// End.
							StartingIndex = 0;
							Draw = True;
						}
					}
				}
			}
		}
			
		foreach (Event in PendingEvents) {
			switch (Event.Type) {
				case CMlScriptEvent::Type::PluginCustomEvent: {
					if (Event.CustomEventType == C_CustomEventType) {
						// The visibility seems to have changed.
						
						declare Boolean Visibility = IsVisible();
						if (SavedVisibility != Visibility) {
							SavedVisibility = Visibility;
							AnimMgr.Flush(ConsoleFrame);
							if (Visibility) {
								// Showing the console.
								StartOverridingEvents();

								AnimMgr.Add(ConsoleFrame, '''<control pos="-160 -30"/>''', C_AnimationDuration, CAnimManager::EAnimManagerEasing::Linear);
							} else {
								// Hiding the console.
								StopOverridingEvents();

								AnimMgr.Add(ConsoleFrame, '''<control pos="-160 -90"/>''', C_AnimationDuration, CAnimManager::EAnimManagerEasing::Linear);
								MoreFrame.Hide();

								ConsoleBgQuad.Focus(); //< Removes the focus from the inputs if any.
							}
						}
					}
				}
				case CMlScriptEvent::Type::MenuNavigation: {
					if (SavedVisibility) {
						declare Integer Lines = 1;
						if (IsCtrlPressed()) Lines = 3;

						switch (Event.MenuNavAction) {
							case CMlScriptEvent::EMenuNavAction::ScrollUp: {
								// Scrolling up.
								declare Integer SavedIndex = StartingIndex;
								StartingIndex = ML::Min(ML::Max(FilteredHistory.count - G_LogLines.count, 0), StartingIndex + Lines);
								if (SavedIndex == StartingIndex) {
									LinesFrame.ScrollBumpTop(); //< not working
								} else {
									Draw = True;
								}
							}
							case CMlScriptEvent::EMenuNavAction::ScrollDown: {
								// Scrolling down.
								declare Integer SavedIndex = StartingIndex;
								StartingIndex = ML::Max(StartingIndex - Lines, 0);
								if (SavedIndex == StartingIndex) {
									LinesFrame.ScrollBumpBottom(); //< not working
								} else {
									Draw = True;
								}
							}
						}
					}
				}
				case CMlScriptEvent::Type::MouseOver: {
					switch (Event.ControlId) {
						case "l-copy": {
							GetBgQuad(Event.Control.Parent.Parent).Opacity = C_BgQuadOpacityFocus;
						}
						case "q-bg": {
							declare CMlQuad BgQuad = (Event.Control as CMlQuad);
							BgQuad.Opacity = C_BgQuadOpacityFocus;
						}
					}
				}
				case CMlScriptEvent::Type::MouseOut: {
					switch (Event.ControlId) {
						case "l-copy": {
							GetBgQuad(Event.Control.Parent.Parent).Opacity = C_BgQuadOpacity;
						}
						case "q-bg": {
							declare CMlQuad BgQuad = (Event.Control as CMlQuad);
							BgQuad.Opacity = C_BgQuadOpacity;
						}
					}
				}
				case CMlScriptEvent::Type::MouseClick: {
					switch (Event.ControlId) {
						case "l-filter-time": {
							AnimMgr.Flush(FilterTimeFrame);
							FilterTimeFrame.Visible = !FilterTimeFrame.Visible;
							FilterTimeFrame.RelativeScale = 0.;
							AnimMgr.Add(FilterTimeFrame, '''<control scale="1"/>''', 100, CAnimManager::EAnimManagerEasing::Linear);
							FilterTypeFrame.Visible = False;
							FilterScriptFrame.Visible = False;
							FilterMessageFrame.Visible = False;
						}
						case "l-filter-time-start-reset": {
							FilterTimeWindowStartEntry.SetText("0", True);
						}
						case "l-filter-time-end-reset": {
							FilterTimeWindowEndEntry.SetText("0", True);
						}
						case "l-filter-scripts-reset": {
							FilterScriptsEntry.SetText("", True);
						}
						case "l-filter-message-regex-reset": {
							FilterMessageRegexEntry.SetText("", True);
						}
						case "l-filter-type": {
							AnimMgr.Flush(FilterTypeFrame);
							FilterTypeFrame.Visible = !FilterTypeFrame.Visible;
							FilterTypeFrame.RelativeScale = 0.;
							AnimMgr.Add(FilterTypeFrame, '''<control scale="1"/>''', 100, CAnimManager::EAnimManagerEasing::Linear);
							FilterTimeFrame.Visible = False;
							FilterScriptFrame.Visible = False;
							FilterMessageFrame.Visible = False;
						}
						case "l-filter-script": {
							AnimMgr.Flush(FilterScriptFrame);
							FilterScriptFrame.Visible = !FilterScriptFrame.Visible;
							FilterScriptFrame.RelativeScale = 0.;
							AnimMgr.Add(FilterScriptFrame, '''<control scale="1"/>''', 100, CAnimManager::EAnimManagerEasing::Linear);
							FilterTimeFrame.Visible = False;
							FilterTypeFrame.Visible = False;
							FilterMessageFrame.Visible = False;
						}
						case "l-filter-message": {
							AnimMgr.Flush(FilterMessageFrame);
							FilterMessageFrame.Visible = !FilterMessageFrame.Visible;
							FilterMessageFrame.RelativeScale = 0.;
							AnimMgr.Add(FilterMessageFrame, '''<control scale="1"/>''', 100, CAnimManager::EAnimManagerEasing::Linear);
							FilterTimeFrame.Visible = False;
							FilterTypeFrame.Visible = False;
							FilterScriptFrame.Visible = False;
						}
						case "l-scroll-lock": {
							G_ScrollLock = !G_ScrollLock;
							(Event.Control as CMlLabel).SetText(C_ScrollLockIcons[G_ScrollLock]);
						}
						case "l-more": {
							declare Integer FilteredHistoryRefIndex for Event.Control.Parent.Parent;
							if (FilteredHistoryRefIndex > -1) {
								MoreTextEdit.Value = FilteredHistory[FilteredHistoryRefIndex].Message;
								MoreInfoLabel.Value = "$o" ^ TypeToText(FilteredHistory[FilteredHistoryRefIndex].Type) ^
									"$z\tat\t$o" ^ FormatTime(FilteredHistory[FilteredHistoryRefIndex].Time) ^
									"$z\tby\t$o" ^ FilteredHistory[FilteredHistoryRefIndex].Script ^
									"$z\t\t$o" ^ CountOccurrences(FilteredHistory[FilteredHistoryRefIndex].Message, "\n") ^ "$z lines" ^
									"$z\t\t$o" ^ TL::Length(FilteredHistory[FilteredHistoryRefIndex].Message) ^ "$z characters";
								MoreFrame.Show();
								AnimMgr.Flush(MoreFrame);
								MoreFrame.RelativeScale = 0.;
								AnimMgr.Add(MoreFrame, '''<control scale="1"/>''', 100, CAnimManager::EAnimManagerEasing::Linear);
							}
						}
						case "l-more-close": {
							MoreFrame.Hide();
						}
						case "l-copy": {
							declare Integer FilteredHistoryRefIndex for Event.Control.Parent.Parent;
							if (FilteredHistoryRefIndex > -1) {
								declare Text NewClipboard = FilteredHistory[FilteredHistoryRefIndex].Message;
								if (IsCtrlPressed()) {
									NewClipboard = TypeToText(FilteredHistory[FilteredHistoryRefIndex].Type) ^ " | " ^
										FilteredHistory[FilteredHistoryRefIndex].Time ^ " | " ^
										FilteredHistory[FilteredHistoryRefIndex].Script ^ " > " ^
										NewClipboard;
								}
								System.ClipboardSet(NewClipboard);
							}
						}
					}

					if (Event.Control != Null && Event.Control.HasClass("l-btn")) {
						declare CMlLabel Label = (Event.Control as CMlLabel);
						AnimMgr.Flush(Label);
						Label.Scale = C_LabelScaleClick;
						Label.Opacity = C_LabelOpacityClick;
						AnimMgr.Add(Label, '''<control opacity="<<< C_LabelOpacity >>>" scale="<<< C_LabelScale >>>"/>''', 500, CAnimManager::EAnimManagerEasing::Linear);
					}
				}
				case CMlScriptEvent::Type::EntrySubmit: {
					switch (Event.ControlId) {
						case "e-filter-time-start": {
							declare Text Value = (Event.Control as CMlEntry).Value;
							declare Integer TimeWindowStart;
							if (Value != "" && TL::RegexFind("^\\d+$", Value, "").count == 0) {
								Log_Error(C_ScriptName, "Wrong time filter start value");
								TimeWindowStart = 0;
							} else {
								TimeWindowStart = TL::ToInteger(Value);
							}
							SetFilterTimeWindowStart(TimeWindowStart);
							FilterTimeActiveLabel.Visible = (TimeWindowStart != 0 || GetFilterTimeWindowEnd() != 0);
							Compute = True;
						}
						case "e-filter-time-end": {
							declare Text Value = (Event.Control as CMlEntry).Value;
							declare Integer TimeWindowEnd;
							if (Value != "" && TL::RegexFind("^\\d+$", Value, "").count == 0) {
								Log_Error(C_ScriptName, "Wrong time filter end value");
								TimeWindowEnd = 0;
							} else {
								TimeWindowEnd = TL::ToInteger(Value);
							}
							SetFilterTimeWindowEnd(TimeWindowEnd);
							FilterTimeActiveLabel.Visible = (GetFilterTimeWindowStart() != 0 || TimeWindowEnd != 0);
							Compute = True;
						}
						case "e-filter-scripts": {
							declare Text ScriptsValue = (Event.Control as CMlEntry).Value;
							declare Text FilterScripts;
							if (ScriptsValue != "" && TL::RegexFind("^\\w+(?:,\\w+)*$", ScriptsValue, "").count == 0) {
								Log_Error(C_ScriptName, "Wrong scripts filter value");
								FilterScripts = "";
							} else {
								FilterScripts = ScriptsValue;
							}
							SetFilterScripts(FilterScripts);
							FilterScriptsActiveLabel.Visible = (FilterScripts != "");
							Compute = True;
						}
						case "e-filter-message-regex": {
							declare Text FilterMessagePattern = (Event.Control as CMlEntry).Value;
							SetFilterMessagePattern(FilterMessagePattern);
							FilterMessageActiveLabel.Visible = (FilterMessagePattern != "");
							Compute = True;
						}
						// case "e-cmd": {
						// 	declare Text Value = CommandEntry.Value;
						// 	if (Value != "") {
						// 		CommandHistory.addfirst(Value);
						// 		declare Text[] CommandElements = TL::RegexFind('''("[^"]*"|\S+)''', Value, "g");
						// 		SendCustomEvent(C_DispatchCommand, CommandElements);
						// 		CommandEntry.Value = "";
						// 		CommandEntry.StartEdition();
						// 	}
						// }
					}
				}
			}
		}

		SavedCommandEntryFocus = CommandEntry.IsFocused;
		***
		
		CMlLabel GetTimeLabel(CMlFrame _LogLineFrame) {
			return GetLabelById(_LogLineFrame, "l-time");
		}
		
		CMlLabel GetTypeLabel(CMlFrame _LogLineFrame) {
			return GetLabelById(_LogLineFrame, "l-type");
		}

		CMlLabel GetScriptLabel(CMlFrame _LogLineFrame) {
			return GetLabelById(_LogLineFrame, "l-script");
		}

		CMlLabel GetMessageLabel(CMlFrame _LogLineFrame) {
			return GetLabelById(_LogLineFrame, "l-message");
		}

		CMlLabel GetCopyLabel(CMlFrame _LogLineFrame) {
			return GetLabelById(_LogLineFrame, "l-copy");
		}

		CMlLabel GetLinesLabel(CMlFrame _LogLineFrame) {
			return GetLabelById(_LogLineFrame, "l-lines");
		}

		CMlLabel GetMoreLabel(CMlFrame _LogLineFrame) {
			return GetLabelById(_LogLineFrame, "l-more");
		}

		CMlQuad GetLineQuad(CMlFrame _LogLineFrame) {
			return GetQuadById(_LogLineFrame, "q-line");
		}

		CMlQuad GetBgQuad(CMlFrame _LogLineFrame) {
			return GetQuadById(_LogLineFrame, "q-bg");
		}

		Text CharAt(Text _String, Integer _Index) {
			return TL::SubText(_String, _Index, 1);
		}

		Integer CountOccurrences(Text _Haystack, Text _Needle) {
			return TL::RegexFind(_Needle, _Haystack, "g").count + 1;
		}

		Integer GetFilterTimeWindowStart() {
			declare Integer Unbitn_TPEngine_Console_FilterTimeWindowStart as TimeWindowStart for LoadedTitle;
			return TimeWindowStart;
		}
		
		Void SetFilterTimeWindowStart(Integer _TimeWindowStart) {
			declare Integer Unbitn_TPEngine_Console_FilterTimeWindowStart as TimeWindowStart for LoadedTitle;
			TimeWindowStart = _TimeWindowStart;
		}

		Integer GetFilterTimeWindowEnd() {
			declare Integer Unbitn_TPEngine_Console_FilterTimeWindowEnd as TimeWindowEnd for LoadedTitle;
			return TimeWindowEnd;
		}

		Void SetFilterTimeWindowEnd(Integer _TimeWindowEnd) {
			declare Integer Unbitn_TPEngine_Console_FilterTimeWindowEnd as TimeWindowEnd for LoadedTitle;
			TimeWindowEnd = _TimeWindowEnd;
		}

		Boolean IsFilterTypeLogDisabled() {
			declare Boolean Unbitn_TPEngine_Console_FilterTypeLogDisabled as TypeLogDisabled for LoadedTitle;
			return TypeLogDisabled;
		}

		Void SetFilterTypeLogDisabled(Boolean _TypeLogDisabled) {
			declare Boolean Unbitn_TPEngine_Console_FilterTypeLogDisabled as TypeLogDisabled for LoadedTitle;
			TypeLogDisabled = _TypeLogDisabled;
		}

		Boolean IsFilterTypeWarnDisabled() {
			declare Boolean Unbitn_TPEngine_Console_FilterTypeWarnDisabled as TypeWarnDisabled for LoadedTitle;
			return TypeWarnDisabled;
		}

		Void SetFilterTypeWarnDisabled(Boolean _TypeWarnDisabled) {
			declare Boolean Unbitn_TPEngine_Console_FilterTypeWarnDisabled as TypeWarnDisabled for LoadedTitle;
			TypeWarnDisabled = _TypeWarnDisabled;
		}

		Boolean IsFilterTypeErrorDisabled() {
			declare Boolean Unbitn_TPEngine_Console_FilterTypeErrorDisabled as TypeErrorDisabled for LoadedTitle;
			return TypeErrorDisabled;
		}

		Void SetFilterTypeErrorDisabled(Boolean _TypeErrorDisabled) {
			declare Boolean Unbitn_TPEngine_Console_FilterTypeErrorDisabled as TypeErrorDisabled for LoadedTitle;
			TypeErrorDisabled = _TypeErrorDisabled;
		}

		Text GetFilterScripts() {
			declare Text Unbitn_TPEngine_Console_FilterScripts as Scripts for LoadedTitle;
			return Scripts;
		}

		Void SetFilterScripts(Text _Scripts) {
			declare Text Unbitn_TPEngine_Console_FilterScripts as Scripts for LoadedTitle;
			Scripts = _Scripts;
		}

		Text GetFilterMessagePattern() {
			declare Text Unbitn_TPEngine_Console_FilterMessagePattern as MessagePattern for LoadedTitle;
			return MessagePattern;
		}

		Void SetFilterMessagePattern(Text _MessagePattern) {
			declare Text Unbitn_TPEngine_Console_FilterMessagePattern as MessagePattern for LoadedTitle;
			MessagePattern = _MessagePattern;
		}

		Boolean IsVisible() {
			declare Boolean Unbitn_TPEngine_Console_Visibility as Visibility for LoadedTitle;
			return Visibility;
		}
		
		Text TypeToText(Integer _Type) {
			switch (_Type) {
				case C_Log_LogType_Log: return "Log";
				case C_Log_LogType_Warn: return "Warn";
				case C_Log_LogType_Error: return "Error";
			}
			return "";
		}

		Vec3 TypeToTextColor(Integer _Type) {
			switch (_Type) {
				case C_Log_LogType_Log: return <1., 1., 1.>;
				case C_Log_LogType_Warn: return <0., 0., 0.>;
				case C_Log_LogType_Error: return <0., 0., 0.>;
			}
			return <1., 1., 1.>;
		}

		Vec3 TypeToBgColor(Integer _Type) {
			switch (_Type) {
				case C_Log_LogType_Log: return <0., 0., 0.>;
				case C_Log_LogType_Warn: return <0.8, 0.5, 0.>;
				case C_Log_LogType_Error: return <0.8, 0., 0.>;
			}
			return <0., 0., 0.>;
		}

		Text FormatTime(Integer _Time) {
			return (_Time / 1000000) ^ "." ^ TL::FormatInteger((_Time / 1000) % 1000, 3) ^ "." ^ TL::FormatInteger(_Time % 1000, 3);
		}

		Boolean IsCtrlPressed() {
			return (Input.IsKeyPressed(C_CtrlLeft) || Input.IsKeyPressed(C_CtrlRight));
		}

		Boolean SatisfiesTimeFilter(Integer _Time) {
			declare Integer FilterTimeWindowEnd = GetFilterTimeWindowEnd();
			return GetFilterTimeWindowStart() <= _Time && 
				(FilterTimeWindowEnd <= 0 || _Time < FilterTimeWindowEnd);
		}

		Boolean SatisfiesTypeFilter(Integer _Type) {
			return _Type == C_Log_LogType_Log && !IsFilterTypeLogDisabled() ||
				_Type == C_Log_LogType_Warn && !IsFilterTypeWarnDisabled() ||
				_Type == C_Log_LogType_Error && !IsFilterTypeErrorDisabled();
		}

		Boolean SatisfiesScriptFilter(Text _Script) {
			declare Text FilterScripts = GetFilterScripts();
			return FilterScripts == "" || TL::Split(",", TL::ToLowerCase(FilterScripts)).exists(TL::ToLowerCase(_Script));
		}
		
		Boolean SatisfiesMessageFilter(Text _Message) {
			declare Text FilterMessagePattern = GetFilterMessagePattern();
			declare Text[] RegexResult = TL::RegexMatch(FilterMessagePattern, _Message, "i");
			return FilterMessagePattern == "" || (RegexResult.count > 0 && RegexResult[0] != "");
		}
		
		Boolean SatisfiesFilters(M_LogLine _LogLine) {
			return SatisfiesTimeFilter(_LogLine.Time) &&
				SatisfiesTypeFilter(_LogLine.Type) &&
				SatisfiesScriptFilter(_LogLine.Script) &&
				SatisfiesMessageFilter(_LogLine.Message);
		}

		M_LogLine[] Filter(M_LogLine[] _History) {
			declare M_LogLine[] FilteredHistory;
			foreach (Line in _History) {
				if (SatisfiesFilters(Line)) FilteredHistory.add(Line);
			}
			return FilteredHistory;
		}

		Void StartOverridingEvents() {
			EnableMenuNavigation(True, False, True, Null, C_MaxInt);
		}

		Void StopOverridingEvents() {
			EnableMenuNavigation(True, False, False, Null, C_MinInt);
		}
	]]></Maniascript>
</View>
""";

	G_Subscription = EventMgr::Subscribe(EEventType::C_CommandDispatched);
	
	return Response::New(
		View::New(
			C_Id,
			View,
			True
		),
		EStack::C_EngineConsole,
		EDisplayMethod::C_Set
	);
}

Void Loop() {
	foreach (Event in EventMgr::GetSubscribedEvents(G_Subscription)) {
		declare M_CommandDispatchedEventData EventData = CommandDispatchedEventData::Deserialize(Event.Data);
		if (EventData.Name == C_CommandName) {
			declare Text[] Args = EventData.Args;
			
			if (Args.count == 0) {
				Log::Error(C_ScriptName, """Missing argument for {{{ dump(C_CommandName)}}} command.""");
			} else {
				declare Text SubCommand = Args[0];
				declare IgnoreWarnings = Args.removekey(0);

				switch (SubCommand) {
					case "help": {
						Log::Log(C_ScriptName, "Available subcommands: help, toggle");
					}

					case "toggle": {
						Toggle();
					}

					default: {
						Log::Error(C_ScriptName, """Unknown {{{ dump(SubCommand) }}} subcommand.""");
					}
				}
			}

			EventMgr::Consume(Event);
		}
	}
	
	foreach (Event in PendingEvents) {
		switch (Event.Type) {
			case CManiaAppEvent::EType::KeyPress: {
				if (Event.KeyName == "D" && Input.IsKeyPressed(C_CtrlLeft)) {
					Toggle();
				}
			}
			case CManiaAppEvent::EType::LayerCustomEvent: {
				if (Event.CustomEventType == C_DispatchCommand && Event.CustomEventData.count > 0) {
					declare Text CommandName = Event.CustomEventData[0];
					declare Text[] CommandArgs;
					for (I, 1, Event.CustomEventData.count - 1) {
						declare TL::M_RegexMatchResult RegexResult = TL::RegexMatch("""^"(.*)"$""", Event.CustomEventData[I], "");
						if (RegexResult.Groups.existskey(0)) {
							CommandArgs.add(RegexResult.Groups[0]);
						} else {
							CommandArgs.add(Event.CustomEventData[I]);
						}
					}

					EventMgr::Dispatch(Event::New(
						EEventType::C_CommandDispatched,
						True,
						CommandDispatchedEventData::New(CommandName, CommandArgs).tojson()
					));
				}
			}
		}
	}
}