#Include "Libs/Unbitn/Engine/Log/Log.Script.txt" as Log
#Include "Libs/Unbitn/Engine/Env/Enums/Environment.Script.txt" as EEnvironment
#Include "Libs/Unbitn/Engine/Settings/Settings.Script.txt" as Settings
#Include "Libs/Unbitn/Engine/EventManager/EventManager.Script.txt" as EventMgr
#Include "Libs/Unbitn/Engine/AccessControl/AccessControl.Script.txt" as AccessControl
#Include "Libs/Unbitn/Engine/EventManager/Models/Event.Script.txt" as Event
#Include "Libs/Unbitn/Engine/Router/Models/NavigatedEventData.Script.txt" as NavigatedEventData
#Include "Libs/Unbitn/Engine/Router/Enums/EventType.Script.txt" as RouterEvents
#Include "Libs/Unbitn/Engine/Console/Enums/EventType.Script.txt" as EConsoleEventType

#Include "Libs/Unbitn/Engine/Console/Models/CommandDispatchedEventData.Script.txt" as CommandDispatchedEventData
#Struct CommandDispatchedEventData::M_CommandDispatchedEventData as M_CommandDispatchedEventData

#Include "Libs/Unbitn/Engine/Router/Models/Route.Script.txt" as Route
#Struct Route::M_Route as M_Route

#Include "Libs/Unbitn/Engine/Router/Models/NavigateEventData.Script.txt" as NavigateEventData
#Struct NavigateEventData::M_NavigateEventData as M_NavigateEventData

#Const C_ScriptName		"Router"
#Const C_CommandName	"router"

declare Text G_Subscription;

/**
 * The current position in the route history.
 */
declare Integer G_CurrentHistoryPosition;

/**
 * History of the routes.
 */
declare M_Route[] G_History;

/**
 * Sets the route and dispatches a Navigated event.
 * @param _Route The route to set.
 */
Void Private_SetRoute(M_Route _Route) {
	declare M_Route Unbitn_TPEngine_Router_CurrentRoute as CurrentRoute for LoadedTitle;
	CurrentRoute = _Route;

	declare Integer Unbitn_TPEngine_Router_LastRouteTime as LastRouteTime for LoadedTitle;
	LastRouteTime = Now;

	Log::Log(C_ScriptName, """Navigated to "{{{_Route.Controller}}}/{{{_Route.Action}}}".""");
	
	EventMgr::Dispatch(Event::New(
		RouterEvents::C_Navigated,
		False,
		NavigatedEventData::New(CurrentRoute).tojson()
	));
}

/**
 * Sets the route.
 * Generates a NavigatedEvent.
 * @param _Route The route to set.
 */
Void SetRoute(M_Route _Route) {
	declare M_Route Route = _Route;
	if (!AccessControl::IsUserAuthorized(Route.Controller, Route.Action)) {
		Route = AccessControl::GetRedirection(Route.Controller, Route.Action);

		// If no route specified, then no redirection.
		if (Route.Controller == "") return;
	}

	Private_SetRoute(Route);

	// Adds the current route to the history.
	if (!G_History.existskey(0) || !Route::Equals(G_History[0], Route)) {
		for (I, 0, G_CurrentHistoryPosition - 1) declare Value = G_History.removekey(0);
		G_History.addfirst(Route);
		G_CurrentHistoryPosition = 0;
	}
}

/**
 * Sets the route.
 * Generates a NavigatedEvent.
 * @param _Controller The route's controller.
 * @param _Action The new route's action.
 * @param _Parameters Additional data to pass to the action.
 */
Void SetRoute(Text _Controller, Text _Action, Text[Text] _Parameters) {
	SetRoute(Route::New(_Controller, _Action, _Parameters));
}

/**
 * Sets the route.
 * Generates a NavigatedEvent.
 * @param _Controller The route's controller.
 * @param _Action The new route's action.
 */
Void SetRoute(Text _Controller, Text _Action) {
	SetRoute(_Controller, _Action, []);
}

/**
 * Sets the route.
 * Generates a NavigatedEvent.
 * @param _Controller The route's controller.
 * @param _Action The new route's action.
 */
Void SetRoute(Text _Controller) {
	SetRoute(_Controller, "");
}

/**
 * Checks if a previous route exists in the history.
 */
Boolean CanGoBack() {
	return G_History.existskey(G_CurrentHistoryPosition + 1);
}

/**
 * Gets to the previous route with the history.
 */
Void Back() {
	assert(CanGoBack(), "Can't go back to the previous route.");
	G_CurrentHistoryPosition += 1;
	Log::Log(C_ScriptName, "Going back.");
	Private_SetRoute(G_History[G_CurrentHistoryPosition]);
}

/**
 * Checks if a next route exists in the history.
 */
Boolean CanGoForward() {
	return G_History.existskey(G_CurrentHistoryPosition - 1);
}

/**
 * Gets to the next route with the history.
 */
Void Forward() {
	assert(CanGoForward(), "Can't go forward to the next route.");
	G_CurrentHistoryPosition -= 1;
	Log::Log(C_ScriptName, "Going forward.");
	Private_SetRoute(G_History[G_CurrentHistoryPosition]);
}

/**
 * Gets the current route.
 * @return The current route.
 */
M_Route GetCurrentRoute() {
	declare M_Route Unbitn_TPEngine_Router_CurrentRoute for LoadedTitle;
	return Unbitn_TPEngine_Router_CurrentRoute;
}

/**
 * Gets the current route's controller.
 * @return The controller of the current route.
 */
Text GetCurrentController() {
	return GetCurrentRoute().Controller;
}

/**
 * Gets the current route's action.
 * @return The action of the current route.
 */
Text GetCurrentAction() {
	return GetCurrentRoute().Action;
}

/**
 * Gets the current route's additional data.
 * @return The additional data of the current route.
 */
Text[Text] GetParameters() {
	return GetCurrentRoute().Parameters;
}

/**
 * Gets the history of the navigated routes.
 * @return The route history.
 */
M_Route[] GetHistory() {
	return G_History;
}

/**
 * Gets the current position in the history.
 * @return The current position in the history.
 */
Integer GetCurrentHistoryPosition() {
	return G_CurrentHistoryPosition;
}

/**
 * Handles Navigate events.
 */
Void Loop() {
	foreach (Event in EventMgr::GetSubscribedEvents(G_Subscription)) {
		declare M_CommandDispatchedEventData EventData = CommandDispatchedEventData::Deserialize(Event.Data);
		
		if (EventData.Name == C_CommandName) {
			declare Text[] Args = EventData.Args;

			if (Args.count == 0) {
				Log::Error(C_ScriptName, """Missing argument for {{{ dump(C_CommandName)}}} command.""");
			} else {
				declare Text SubCommand = Args[0];
				declare IgnoreWarnings = Args.removekey(0);
				declare Text Env = Settings::Get().Env;

				switch (SubCommand) {
					case "help": {
						Log::Log(C_ScriptName, "Available subcommands: back, forward, get, help, set");
					}

					case "back": {
						if (Env != EEnvironment::C_Development) {
							Log::Error(C_ScriptName, """The "back" subcommand is not available in {{{ dump(Env) }}} mode.""");
						} else {
							if (!CanGoBack()) {
								Log::Error(C_ScriptName, "Can't go back to the previous route.");
							} else {
								Back();
							}
						}
					}

					case "forward": {
						if (Env != EEnvironment::C_Development) {
							Log::Error(C_ScriptName, """The "forward" subcommand is not available in {{{ dump(Env) }}} mode.""");
						} else {
							if (!CanGoForward()) {
								Log::Error(C_ScriptName, "Can't go forward to the next route.");
							} else {
								Forward();
							}
						}
					}

					case "get": {
						declare M_Route Route = GetCurrentRoute();
						Log::Log(C_ScriptName, """Current route: {{{ Route.Controller }}}/{{{ Route.Action }}} with parameters: {{{ dump(Route.Parameters) }}}.""");
					}

					case "set": {
						if (Env != EEnvironment::C_Development) {
							Log::Error(C_ScriptName, """The "set" subcommand is not available in {{{ dump(Env) }}} mode.""");
						} else {
							switch (Args.count) {
								case 0: {
									Log::Error(C_ScriptName, """Missing controller and action arguments for "set" subcommand.""");
								}
								case 1: {
									declare Text Controller = Args[0];
									SetRoute(Controller);
								}
								default: {
									declare Text Controller = Args[0];
									declare Text Action = Args[1];
									SetRoute(Controller, Action);
								}
							}
						}
					}

					default: {
						Log::Error(C_ScriptName, """Unknown {{{ dump(SubCommand) }}} subcommand.""");
					}
				}
			}

			EventMgr::Consume(Event);
		}
	}

	foreach (Event in PendingEvents) {
		if (Event.Type == CManiaAppEvent::EType::LayerCustomEvent) {
			if (Event.CustomEventType == RouterEvents::C_Navigate) {
				assert(Event.CustomEventData.count > 0, """Missing data for a Navigate event.""");
				declare M_NavigateEventData NavigateEventData;
				assert(NavigateEventData.fromjson(Event.CustomEventData[0]), "An error occurred while unserializing the event data.");
				
				SetRoute(NavigateEventData.Route.Controller, NavigateEventData.Route.Action, NavigateEventData.Route.Parameters);
			}
		}
	}
}

Void Load() {
	G_Subscription = EventMgr::Subscribe(EConsoleEventType::C_CommandDispatched);
}
