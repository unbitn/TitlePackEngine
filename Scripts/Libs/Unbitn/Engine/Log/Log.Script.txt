#Include "Libs/Unbitn/TextLib.Script.txt" as TL
#Include "Libs/Unbitn/Engine/Env/Env.Script.txt" as Env
#Include "Libs/Unbitn/Engine/Env/Enums/Environment.Script.txt" as Environment

#Include "Libs/Unbitn/Engine/Log/Enums/LogType.Script.txt" as LogType

#Include "Libs/Unbitn/Engine/Log/Models/LogLine.Script.txt" as LogLine
#Struct LogLine::M_LogLine as M_LogLine

#Const C_ScriptName "Log"

#Const C_Log					"LOG"
#Const C_Warn					"WARN"
#Const C_Error				"ERROR"

Void Private_WriteLine(Integer _Time, Text _TypeAsString, Text _Script, Text _Message) {
	log(TL::Compose(
		"%1 | %2 | %3 > %4",
		[_TypeAsString, ""^_Time, _Script, _Message],
		True
	));
}

Void Private_SetLogWarning(Boolean _WarningSent) {
	declare Boolean Unbitn_TPEngine_Log_LogWarning as LogWarning for LoadedTitle;
	LogWarning = _WarningSent;
}

Boolean Private_IsLogWarningSent() {
	declare Boolean Unbitn_TPEngine_Log_LogWarning as LogWarning for LoadedTitle;
	return LogWarning;
}

Void Private_SetWarnWarning(Boolean _WarningSent) {
	declare Boolean Unbitn_TPEngine_Log_WarnWarning as WarnWarning for LoadedTitle;
	WarnWarning = _WarningSent;
}

Boolean Private_IsWarnWarningSent() {
	declare Boolean Unbitn_TPEngine_Log_WarnWarning as WarnWarning for LoadedTitle;
	return WarnWarning;
}

Void Private_SetErrorWarning(Boolean _WarningSent) {
	declare Boolean Unbitn_TPEngine_Log_ErrorWarning as ErrorWarning for LoadedTitle;
	ErrorWarning = _WarningSent;
}

Boolean Private_IsErrorWarningSent() {
	declare Boolean Unbitn_TPEngine_Log_ErrorWarning as ErrorWarning for LoadedTitle;
	return ErrorWarning;
}

/**
 * Gets all the log lines issued since then.
 * @return The log lines.
 */
M_LogLine[] GetHistory() {
	declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
	return History;
}

/**
 * Prints out a line in the console with the given string.
 * @param _Script The script name.
 * @param _Message The string to write.
 */
Void Log(Text _Script, Text _Message) {
	declare Text Env = Env::Get();
	declare Boolean Unbitn_TPEngine_Log_KeepLogs as KeepLogs for LoadedTitle;

	if (KeepLogs) {
		declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
		History.addfirst(LogLine::New(
			Now,
			LogType::C_Log,
			_Script,
			_Message
		));
	}

	if (Env == Environment::C_Development || Env == Environment::C_Test) {
		Private_WriteLine(Now, C_Log, _Script, _Message);
	} else if (!Private_IsLogWarningSent()) {
		Private_SetLogWarning(True);
		Private_WriteLine(Now, C_Log, C_ScriptName, """Logging is disabled in the {{{ dump(Env) }}} environment.""");
	}
}

/**
 * Prints out a line in the console with the given string if the verbose mode is enabled.
 * @param _Script The script name.
 * @param _Message The string to write.
 */
Void Verbose(Text _Script, Text _Message) {
	declare Boolean Unbitn_TPEngine_Log_VerboseState as VerboseState for LoadedTitle;
	if (VerboseState) Log(_Script, _Message);
}

/**
 * Prints out a warning line in the console with the given string.
 * @param _Script The script name.
 * @param _Message The warning string to write.
 */
Void Warn(Text _Script, Text _Message) {
	declare Text Env = Env::Get();
	declare Boolean Unbitn_TPEngine_Log_KeepWarnings as KeepWarnings for LoadedTitle;
	
	if (KeepWarnings) {
		declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
		History.addfirst(LogLine::New(
			Now,
			LogType::C_Warn,
			_Script,
			_Message
		));
	}

	if (Env == Environment::C_Development || Env == Environment::C_Test) {
		Private_WriteLine(Now, C_Warn, _Script, _Message);
	} else if (!Private_IsWarnWarningSent()) {
		Private_SetWarnWarning(True);
		Private_WriteLine(Now, C_Log, C_ScriptName, """Warning is disabled in the {{{ dump(Env) }}} environment.""");
	}
}

/**
 * Writes an error line in the console with the given string.
 * @param _Script The script name.
 * @param _Message The error string to write.
 */
Void Error(Text _Script, Text _Message) {
	declare Text Env = Env::Get();
	declare Boolean Unbitn_TPEngine_Log_KeepErrors as KeepErrors for LoadedTitle;

	if (KeepErrors) {
		declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
		History.addfirst(LogLine::New(
			Now,
			LogType::C_Error,
			_Script,
			_Message
		));
	}

	if (Env == Environment::C_Development || Env == Environment::C_Test) {
		Private_WriteLine(Now, C_Error, _Script, _Message);
	} else if (!Private_IsErrorWarningSent()) {
		Private_SetErrorWarning(True);
		Private_WriteLine(Now, C_Log, C_ScriptName, """Erroring is disabled in the {{{ dump(Env) }}} environment.""");
	}
}

/**
 * Sets if the verbose logs have to be printed out.
 * @param _State True to print out verbose logs, False otherwise.
 */
Void SetVerboseState(Boolean _State) {
	declare Boolean Unbitn_TPEngine_Log_VerboseState as VerboseState for LoadedTitle;
	VerboseState = _State;

	if (_State) {
		Log(C_ScriptName, "Enabled verbose mode.");
	} else {
		Log(C_ScriptName, "Disabled verbose mode.");
	}
}

/**
 * Gets the state of the verbose.
 * @return True if verbose logs are printed out, False otherwise.
 */
Boolean GetVerboseState() {
	declare Boolean Unbitn_TPEngine_Log_VerboseState as VerboseState for LoadedTitle;
	return VerboseState;
}

/**
 * Sets if the logs have to be saved to access them later with {@code GetLogs()}.
 * @param _State True to keep logs, False otherwise.
 */
Void KeepLogs(Boolean _State) {
	declare Boolean Unbitn_TPEngine_Log_KeepLogs as KeepLogs for LoadedTitle;
	KeepLogs = _State;
	
	if (_State) {
		Log(C_ScriptName, "Enabled logs keeping.");
	} else {
		Log(C_ScriptName, "Disabled logs keeping.");
	}
}

/**
 * Checks if the logs are kept.
 * @return True if the logs are kept, False otherwise.
 */
Boolean AreLogsKept() {
	declare Boolean Unbitn_TPEngine_Log_KeepLogs as KeepLogs for LoadedTitle;
	return KeepLogs;
}

/**
 * Sets if the warnings have to be saved to access them later with {@code GetWarnings()}.
 * @param _State True to keep warnings, False otherwise.
 */
Void KeepWarnings(Boolean _State) {
	declare Boolean Unbitn_TPEngine_Log_KeepWarnings as KeepWarnings for LoadedTitle;
	KeepWarnings = _State;
	
	if (_State) {
		Log(C_ScriptName, "Enabled warnings keeping.");
	} else {
		Log(C_ScriptName, "Disabled warnings keeping.");
	}
}

/**
 * Checks if the warnings are kept.
 * @return True if the warnings are kept, False otherwise.
 */
Boolean AreWarningsKept() {
	declare Boolean Unbitn_TPEngine_Log_KeepWarnings as KeepWarnings for LoadedTitle;
	return KeepWarnings;
}

/**
 * Sets if the errors have to be saved to access them later with {@code GetErrors()}.
 * @param _State True to keep errors, False otherwise.
 */
Void KeepErrors(Boolean _State) {
	declare Boolean Unbitn_TPEngine_Log_KeepErrors as KeepErrors for LoadedTitle;
	KeepErrors = _State;
	
	if (_State) {
		Log(C_ScriptName, "Enabled errors keeping.");
	} else {
		Log(C_ScriptName, "Disabled errors keeping.");
	}
}

/**
 * Checks if the errors are kept.
 * @return True if the errors are kept, False otherwise.
 */
Boolean AreErrorsKept() {
	declare Boolean Unbitn_TPEngine_Log_KeepErrors as KeepErrors for LoadedTitle;
	return KeepErrors;
}

/**
 * Unloads the Log library.
 */
Void Unload() {
	declare M_LogLine[] Unbitn_TPEngine_Log_History as History for LoadedTitle;
	History.clear();

	Log(C_ScriptName, "Unloaded.");
}

/**
 * Loads the Log library.
 */
Void Load() {
	Unload();
	
	Private_SetLogWarning(False);
	Private_SetWarnWarning(False);
	Private_SetErrorWarning(False);
	KeepLogs(False);
	KeepErrors(True);
	KeepWarnings(True);
	SetVerboseState(False);

	Log(C_ScriptName, "Loaded.");
}
