#Include "Libs/Unbitn/Guid.Script.txt" as Guid
#Include "Libs/Unbitn/Engine/Json/Json.Script.txt" as Json

#Struct M_NodeToProcessEventData {
	Text Guid;
	Text ViewGuid;
	Text ViewId;
	Integer Depth;
	Text CreatedNodeGuid;
	Text NodeName;
	Text[Text] Attributes;
	Text[Text] DataAttributes;
	Text Content;
	Text[] Children;
}

M_NodeToProcessEventData New(
	Text _ViewGuid,
	Text _ViewId,
	Integer _Depth,
	Text _CreatedNodeGuid,
	Text _NodeName,
	Text[Text] _Attributes,
	Text[Text] _DataAttributes,
	Text _Content,
	Text[] _Children
) {
	return M_NodeToProcessEventData {
		Guid = Guid::New(),
		ViewGuid = _ViewGuid,
		ViewId = _ViewId,
		Depth = _Depth,
		CreatedNodeGuid = _CreatedNodeGuid,
		NodeName = _NodeName,
		Attributes = _Attributes,
		DataAttributes = _DataAttributes,
		Content = _Content,
		Children = _Children
	};
}

M_NodeToProcessEventData Deserialize(Text _Json) {
	declare M_NodeToProcessEventData NodeToProcessEventData;
	declare IgnoreWarnings = Json::MonitorDeserialization(NodeToProcessEventData.fromjson(_Json), "M_NodeToProcessEventData");
	return NodeToProcessEventData;
}
