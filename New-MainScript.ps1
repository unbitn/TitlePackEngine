Param (
	[string] $Namespace,
	[string] $ScriptName
)

Import-Module ./Utils.psm1

# Namespace name.
if ($Namespace -eq "") {
	$Namespace = Get-Namespace
}

if (-Not (Test-Namespace -Namespace $Namespace)) {
	Write-Error -Message "'$Namespace' is not a valid namespace." -Category SyntaxError
	exit
}

# Main script name.
if ($ScriptName -eq "") {
	$ScriptName = Read-Host "Enter the name of the main script (default: Main)"
	if ($ScriptName -eq "") {
		$ScriptName = "Main"
	}
}

if ($ScriptName -cnotmatch "^[A-Z]\w*$") {
	Write-Error -Message "'$ScriptName' is not a valid script name." -Category SyntaxError
	exit
}

$full_path = "Scripts/Libs/$Namespace/$ScriptName.Script.txt"
$file_content = '/**
 * Generated main script #Namespace#/#ScriptName#.Script.txt
 */
#Extends "Libs/Unbitn/Engine/AppBase.Script.txt"

#Include "Libs/Unbitn/Engine/Router/Enums/EventType.Script.txt" as ERouterEventType

#Include "Libs/Unbitn/Engine/ResponseHandler/Models/Response.Script.txt" as Response
#Struct Response::M_Response as M_Response

#Include "Libs/Unbitn/Engine/Router/Models/NavigatedEventData.Script.txt" as NavigatedEventData
#Struct NavigatedEventData::M_NavigatedEventData as M_NavigatedEventData

***Init***
***
declare Text NavigatedEventsSubscription = EventMgr::Subscribe(ERouterEventType::C_Navigated);
***

***Loop***
***
foreach (Event in EventMgr::GetSubscribedEvents(NavigatedEventsSubscription)) {
	// Navigated to a new action.
	declare M_NavigatedEventData EventData = NavigatedEventData::Deserialize(Event.Data);

	declare M_Response Response;
	switch (EventData.Route.Controller) {
		default: {
			assert(False, """Unknown route {{{ dump(EventData.Route.Controller ^ "/" ^ EventData.Route.Action) }}}.""");
		}
	}

	ResponseHandler::Handle(Response);
	EventMgr::Consume(Event);
}
***
'
$file_content = $file_content -replace '#Namespace#', $Namespace
$file_content = $file_content -replace '#ScriptName#', $ScriptName

if ([System.IO.File]::Exists($full_path)) {
	Write-Error -Message "The file '$full_path' already exists." -Category ResourceExists
	exit
}

Write-Host "Creating main script file '$full_path'."

# Creates the folders.
$folder = $full_path | Split-Path
if (-not (Test-Path $folder)) {
	New-Item -Path $folder -ItemType Directory | Out-Null
}

# Creates the file.
New-Item -Path $full_path -ItemType File -Value $file_content | Out-Null

Write-Host "Main script '$ScriptName' successfully created."
