Param (
	[string] $LocaleId,
	[string] $TranslationId,
	[string] $Value,
	[string] $Namespace
)

Set-StrictMode -Version 3.0
Import-Module ./Utils.psm1

if ($Namespace -eq "") {
	$Namespace = Get-Namespace
}

if (-Not (Test-Namespace -Namespace $Namespace)) {
	Write-Error -Message "'$Namespace' is not a valid namespace." -Category SyntaxError
	exit
}

if ($LocaleId -eq "") {
	$LocaleId = Read-Host "Enter the locale id (default: en)"

	if ($LocaleId -eq "") {
		$LocaleId = "en"
	}
}

if ($TranslationId -eq "") {
	$TranslationId = Read-Host "Enter the translation id"
}

if (-Not (Test-TranslationId -Id $TranslationId)) {
	Write-Error -Message "'$TranslationId' is not a valid translation id." -Category SyntaxError
	exit
}

if ($Value -eq "") {
	$Value = Read-Host "Enter the translated text"
}

$file_path = "Media/Json/$Namespace/Locales/$LocaleId.json"

if (Test-Path -Path $file_path) {
	# File exists.
	$file_content = Get-Content -Path $file_path -Raw

	$match = [regex]::Match($file_content, '^(\s*)"Translations"\s*:\s*\{', [System.Text.RegularExpressions.RegexOptions]::Multiline)

	if (-Not ($match.Success)) {
		Write-Error -Message "Couldn't add the translation." -Category ObjectNotFound
		exit
	}

	$tabs = ($match.Groups[1].Value[0] -as [string]) * ($match.Groups[1].Length + 1)
	
	# Composes the line.
	$new_line = "`n${tabs}`"${TranslationId}`": `"$Value`","
	$insert_pos = $match.Index + $match.Length

	# Merges the content with the new line.
	$file_content = $file_content.Substring(0, $insert_pos) + $new_line + $file_content.Substring($insert_pos)

	# Overwrites the file's content.
	Set-Content -Path $file_path -Value $file_content -NoNewline

} else {
	# File does not exist.
	$file_content = '{
	"$schema": "../../../L10n.schema.json",
	"Id": "#LOCALE_ID#",
	"Translations": {
		"#TRANSLATION_ID#": "#VALUE#"
	}
}'
	$file_content = $file_content -creplace '#LOCALE_ID#', $LocaleId
	$file_content = $file_content -creplace '#TRANSLATION_ID#', $TranslationId
	$file_content = $file_content -creplace '#VALUE#', $Value
	New-Item -Force -Path $file_path -ItemType File -Value $file_content | Out-Null
}
